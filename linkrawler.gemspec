# encoding: utf-8
$:.push File.expand_path("../lib",__FILE__)
require "linkrawler/version"

Gem::Specification.new do |s|
  s.name = "linkrawler"
  s.version = Linkrawler::VERSION
  s.platform = Gem::Platform::RUBY
  s.authors = ["Minjie Zha"]
  s.email = ["minjiezha@gmail.com"]
  s.homepage = "https://bitbucket.org/magic003/linkrawler"
  s.summary = %q{Linkrawler}
  s.description = %q{Linkrawler retrieves links from various link services.}

  s.add_runtime_dependency "yajl-ruby"
  s.add_runtime_dependency "sequel"
  s.add_runtime_dependency "mysql2"
  s.add_runtime_dependency "rack"
  s.add_runtime_dependency "thin"
  s.add_development_dependency "yard"
  s.add_development_dependency "rack-test"

  s.files = Dir['Rakefile', 'bin/linkrawler', '{lib,test}/**/*', 'README*', '.yardopts']
  s.executables = ["linkrawler"]
  s.test_files = Dir['test/**/*']
  s.require_paths = ["lib"]
end
