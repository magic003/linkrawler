require 'linkrawler/tasks/loader'
require 'linkrawler/tasks/task'

module Linkrawler
  # The +Tasks+ module contains tasks that retrieve links from link services,
  # and also task loaders that load initial tasks used at the time of starting
  # the application.
  module Tasks
    # This is the exception for loading tasks.
    class TaskLoadError < StandardError ; end

    # This exception is raised when error happens for getting/parsing response.
    class TaskRequestError < StandardError ; end

    autoload :DummyLoader,    'linkrawler/tasks/dummy_loader'
    autoload :DatabaseLoader, 'linkrawler/tasks/database_loader'
    autoload :DeliciousTask,  'linkrawler/tasks/delicious_task'
    autoload :TwitterTask,    'linkrawler/tasks/twitter_task'
    autoload :FacebookTask,   'linkrawler/tasks/facebook_task'
    autoload :ReaditlaterTask,'linkrawler/tasks/readitlater_task'
    autoload :DatabaseTaskFactory,'linkrawler/tasks/database_task_factory'
  end
end
