require 'thread'

module Linkrawler ; module ThreadPool
  # The +FixedThreadPool+ implements a thread pool with fixed number of threads.
  # The number of threads is specified when creating the instance, and doesn't
  # change during the runtime.
  #
  # @example Creates a fixed thread pool, and gets thread from it
  #
  #   thread_pool = FixedThreadPool.new(2)
  #   thread_pool.execute do 
  #     100.times { 10*10 }
  #   end
  #
  #   thread_pool = FixedThreadPool.new(2)
  #   begin
  #     thread_pool.execute! do 
  #       100.times { 10*10 }
  #     end
  #   rescue NoIdleThreadError => e
  #   end
  class FixedThreadPool
    # The +Worker+ is the essential element of the thread pool. It is the 
    # object that actually executes the job.
    class Worker
      # Creates a worker.
      # 
      # @param [FixedThreadPool] pool which thread pool this worker belongs to
      # @param [Object] job an object which responds to {#call}, can be a block,
      #   a proc or a lambda
      def initialize(pool,job=nil)
        @pool = pool
        @job = job
        @thread = Thread.new do
          while true
            if @job.nil?
              sleep
            else
              @job.call
              done
            end
          end
        end
      end
      
      # Calls this worker to run some code.
      #
      # @param [Object] an object which responds to #call, can be a block,
      #   a proc, or a lambda
      # @yield [] a block containing the code for the job
      def execute(&block)
        @job = block
        @thread.run
      end

      private
      # Called after the work is done.
      def done
        @job = nil
        @pool.ready(self)
      end

    end
    # @return [Integer] number of threads
    attr_reader :size
    # Creates a fixed thread pool with +size+ threads.
    #
    # @param [Integer] size number of threads
    def initialize(size)
      @size = size
      # ready workers
      @ready = []
      size.times do 
        @ready << Worker.new(self)
      end
      # busy workers
      @busy = []
      @mutex = Mutex.new
    end
  
    # Executes the code in this thread pool. If there is no worker ready
    # to run the code, this method will be blocked until someone is ready.
    #
    # @yield [nil] the block specified the code to be executed
    def execute(&block)
      wait = true
      while wait
        sleep 0.01 # wait for some time
        @mutex.synchronize do
          w = @ready.pop
          unless w.nil?
            @busy << w
            w.execute(&block)
            wait = false
          end
        end
      end
    end

    # Executes the code in this thread pool. If there is no worker ready
    # to run the code, a {NoIdleThreadError} is raised.
    #
    # @yield [nil] the block specified the code to be executed
    # @raise [NoIdleThreadError] if there is no idle thread
    def execute!(&block)
      @mutex.synchronize do
        w = @ready.pop
        if w.nil?
          raise NoIdleThreadError, "There is no idle thread. Try again later."
        else
          @busy << w
          w.execute(&block)
        end
      end
    end

    # Notifies the thread pool that the worker is idle.
    #
    # @param [Worker] worker worker to be set as ready
    def ready(worker)
      @mutex.synchronize do 
        @busy.delete(worker)
        @ready << worker
      end
    end
  end
end ; end
