module Linkrawler ; module Dispatcher
  # The abstract dispatcher. A dispatcher manages all the {Tasks::Task} in the
  # system and arranges them to be run in a thread pool.
  #
  # To implement a dispatcher, override the following methods:
  # * {#add_tasks}
  # * {#dispatch}
  # * {#stop}
  #
  # @abstract Override this class to implement a dispatcher.
  class Base
    # Creates a dispatcher.
    #
    # @param [Array<Tasks::Task>] tasks an array of tasks
    def initialize(tasks=nil)
      add_tasks(tasks) unless tasks.nil?
    end

    # This method should be implemented to add {Tasks::Task}s to disptacher.
    #
    # @abstract 
    # @param [Tasks::Task,Array<Tasks::Task>] tasks added tasks
    def add_tasks(tasks)
      raise NotImplementedError, "#{self.class} does not support add_tasks"
    end

    #
    # @see #add_tasks
    # 
    def <<(tasks)
      add_tasks(tasks)
    end

    # This method should be implemented to dispatch tasks to a thread pool.
    #
    # @abstract
    # @param [ThreadPool] thread_pool thread pool to run the tasks
    def dispatch(thread_pool)
      raise NotImplementedError, "#{self.class} does not support dispatch"
    end

    # This method should be implemented to stop dispatching.
    #
    # @abstract
    def stop
      raise NotImplementedError, "#{self.class} does not support stop"
    end

    # This method should be implemented to update a task during runtime.
    #
    # @abstract
    # @param [Integer] id task id
    # @param [Hash] hs hash contains task properties to be updated
    # @return [Tasks::Task] the updated task, nil if not found
    def update_task(id, hs)
      raise NotImplementedError, "#{self.class} does not support update_task"
    end
  end
end ; end
