require 'thread'

module Linkrawler ; module Dispatcher
  # The +PriorityDispatcher+ determines which the next task is based on the
  # priority of the task. A bootstrap task has a high priority, so it is 
  # executed first. Other tasks will be executed after all high-priority tasks 
  # are done.
  #
  # If there are high-priority tasks, it dispatches them immediately. For 
  # low-priority ones, it tries to rerun the same task in a fixed interval,
  # which is specified in the constructor. The interval is calculate roughly.
  #
  # @example Creates a priority dispatcher, add tasks to it and dispatch
  #
  #   tasks = []
  #   # load tasks...
  #
  #   dispatcher = PriorityDispatcher.new
  #   dispatcher.add_tasks(tasks)
  #   dispatcher.dispatch(ThreadPool::FixedThreadPool.new(10))
  #   sleep(100)
  #   dispatcher.stop
  class PriorityDispatcher < Base
    include Log

    # Creates a priority dispatcher.
    #
    # @param [Integer] interval interval in seconds to rerun the same task
    # @param [Array<Tasks::Task>] tasks an array of tasks
    def initialize(interval, tasks=nil)
      @interval_seconds = interval
      @high_q = []
      @low_q = []
      @task_m = {}
      @running = false
      @thread = nil
      @mutex = Mutex.new

      add_tasks(tasks) unless tasks.nil?
    end

    # Adds tasks for dispatching.
    #
    # @param [Tasks::Task,Array<Tasks::Task>] tasks added tasks
    def add_tasks(tasks)
      tasks = [tasks] unless tasks.respond_to?(:each)
      wake = false  # whether to wake up the thread after adding tasks
      @mutex.synchronize do
        tasks.each do |t|
          # Logic here is a little tricky: if it is high, append it to the high
          # queue; otherwise, add it at the head of low queue.
          if high?(t)
            wake = true
            @high_q << t
          else
            @low_q.insert(0,t)
          end
          @task_m[t.id] = t
        end
      end
      wake_thread if wake
    end
    
    # Dispatches tasks. 
    #
    # A background thread is created to dispatch tasks one by one, so this 
    # method returns at once. The thread runs forever, to stop it, call {#stop}.
    #
    # @param [ThreadPool] thread_pool thread pool to run the tasks
    def dispatch(thread_pool)
      unless @running # prevents creating multiple threads
        @running = true
        @thread = Thread.new do 
          while @running
            @mutex.synchronize do
              task = @high_q.shift
              begin
                if task.nil?
                  task = @low_q.shift # get from low queue
                  unless task.nil?
                    thread_pool.execute! do
                      task.run
                    end
                    @low_q << task
                  end
                else
                  # Make sure it is a high-priority task, because after running 
                  # it may be changed.
                  if high?(task) 
                    thread_pool.execute! do
                      task.run
                    end
                    @low_q << task
                  else
                    @low_q << task  # add to low queue
                  end
                end
              rescue ThreadPool::NoIdleThreadError
                sleep 5 # try it later
                retry
              end
            end
            sleep interval 
          end
        end
      end
    end

    # Stops dispatching.
    #
    # @note The task dispatching is stopped at once, but for the already
    #   dispatched tasks, they will keep running until finished.
    def stop
      @running = false
      @thread = nil
    end

    # Update a task.
    #
    # @param [Integer] id task id
    # @param [Hash] hs hash contains task properties to be updated
    # @return [Tasks::Task] the updated task, nil if not found
    #
    # @see Tasks::Task#update
    # @see Tasks::DeliciousTask#update
    # @see Tasks::FacebookTask#update
    # @see Tasks::ReaditlaterTask#update
    # @see Tasks::TwitterTask#update
    def update_task(id, hs)
      if @task_m[id].nil?
        nil
      else
        t = @task_m[id]
        t.update(hs)
        t
      end
    end

    private
    # Checks if a task has a high priority.
    #
    # @param [Tasks::Task] task task to be checked
    # @return [Boolean] true if it is a high-priority task
    def high?(task)
      task.bootstrap?
    end
    
    # Returns the interval time to run a task in seconds.
    # 
    # The dispatcher tries to rerun the same task in a fixed interval, so this
    # method calculates the interval based on the task quantity.
    #
    # @return [Integer] interval in seconds
    def interval
      return 0 unless @high_q.empty?
      # rough value is OK, so don't call @mutex.synchronize
      task_num = @high_q.size + @low_q.size
      task_num == 0 ? @interval_seconds : @interval_seconds / task_num
    end

    # Wakes up the dispatching thread.
    def wake_thread
      unless @thread.nil? || !'sleep'.eql?(@thread.status)
        @thread.run
      end
    end
  end
end ; end
