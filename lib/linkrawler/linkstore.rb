require 'linkrawler/linkstore/base'

module Linkrawler
  # The +Linkstore+ module contains various link stores to save links. Links
  # can be saved in different places, such as database and file system.
  module Linkstore
    # This is the generic exception raised by link stores. All exception
    # classes defined in {Linkstore} are descendants of this class.
    class LinkstoreError < StandardError ; end

    # This is the exception raised when the link store is not available.
    class LinkstoreUnavailableError < LinkstoreError ; end

    # This is the exception when links cannot be saved.
    class LinkSaveError < LinkstoreError 
      # @return successfully saved links
      attr_reader :saved
      # @return failed to be saved links
      attr_reader :failed

      def initialize(saved_links, failed_links, message='Failed to save links')
        super(message)
        @saved = saved_links
        @failed = failed_links
      end
    end

    autoload :LoggerLinkstore,    'linkrawler/linkstore/logger_linkstore'
    autoload :DatabaseLinkstore,  'linkrawler/linkstore/database_linkstore'
    autoload :LinkruncherLinkstore, 'linkrawler/linkstore/linkruncher_linkstore'
  end
end
