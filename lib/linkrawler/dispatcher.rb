require 'linkrawler/dispatcher/base'

module Linkrawler
  # The +Dispatcher+ module contains dispatchers that execute tasks with
  # different strategies. For example, {PriorityDispatcher} treats tasks with
  # different priorities, the higher ones are firstly run and the lower ones
  # are run after that.
  module Dispatcher
    autoload :PriorityDispatcher,   'linkrawler/dispatcher/priority_dispatcher'
  end
end
