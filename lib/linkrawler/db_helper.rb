require 'sequel_core'

module Linkrawler
  # This +DbHelper+ module provides some utilities for accessing databases. The
  # Sequel[http://sequel.rubyforge.org/] gem is used for database handlings.
  #
  module DbHelper

    # Connects to the MySQL database.
    #
    # @param [String] database database name
    # @param [String] user user name
    # @param [String] password user password
    # @param [String] host database server host
    # @param [Integer] port database server port
    # @return [Sequel::Database] databse object from Sequel
    def self.connect_to_mysql(database, user, password, host, port)
      Sequel.mysql2(:host=>host, :port=>port, :user=>user, :password=>password,
        :database=>database)
    end

    # Disconnects from the database.
    # @param [Sequel::Database] db a Sequel database object
    # @return [Void]
    def self.disconnect(db)
      db.disconnect unless db.nil?
    end
  end
end
