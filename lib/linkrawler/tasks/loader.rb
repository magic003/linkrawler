module Linkrawler ; module Tasks
  # The abstract base task loader. Task loaders allows the application to
  # preload a bunch of tasks when starting. For instance, a {DummyLoader}
  # loads an empty array of tasks.
  #
  # To implement a task loader, override the following methods:
  # * {#load}
  #
  # @abstract Override this class to implement a task loader.
  class Loader
    # This method should be implemented to load and return tasks.
    # @abstract
    # @yield [Task] a block to execute with each task
    # @return [Array<Task>] an array of tasks
    def load
      raise NotImplementedError, "#{self.class} must implement #load"
    end
  end
end ; end
