module Linkrawler ; module Tasks
  # This is a dummy task loader that returns an empty array of tasks.
  #
  # It is intended for demonstration or test, but should *never* be used in
  # production environment.
  class DummyLoader < Loader
    # Returns an empty array of task.
    #
    # @return [Array<Task>] an empty array
    def load
      []
    end
  end
end ; end
