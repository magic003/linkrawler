require 'yajl'
require 'time'

module Linkrawler ; module Tasks
  # A task that retrieves links from Readitlater[http://readitlaterlist.com].
  #
  # @example Creates a readitlater task and run it
  #
  #   task = Linkrawler::Tasks::ReaditlaterTask.new('apikey',
  #                                                 'username',
  #                                                 'password',
  #                                                 1)
  #   link_store = MockLinkStore.new
  #   task.add_link_store(link_store)
  #   task.after_save do |links|
  #     # handle the saved links
  #   end
  #   task.run
  #
  class ReaditlaterTask < Task
    # Creates a readitlater task.
    #
    # @param [Integer] id task id
    # @param [String] apikey the app's API key
    # @param [String] username user's name
    # @param [String] password user's password
    # @param [Integer] userid user id in this application. Typically used when 
    #   saving a link
    # @param [Integer] since it could be the timestamp at which last request 
    #   is sent or an integer value of the latest retrieved object's id
    def initialize(id, apikey, username, password, userid, since=nil)
      super(id, userid, since)
      @apikey = apikey
      @username = username
      @password = password
      @client = Clients::ReaditlaterClientSingleton.instance
    end

    # Updates the properties of this task.
    #
    # @param [Hash] hs hash of properties to be updated
    # 
    # @option hs [Integer] :userid user id of this task
    # @option hs [Integer] :since timestamp for the last request
    # @option hs [String] :apikey the app's API key
    # @option hs [String] :username user's name
    # @option hs [String] :password user's password
    #
    # @see Task#update
    def update(hs)
      super(hs)
      unless hs[:apikey].nil?
        @apikey = hs[:apikey]
        logger.info "apikey is updated to #{@apikey} in task #{short_inspect()}"
      end
      unless hs[:username].nil?
        @username = hs[:username]
        logger.info "username is updated to #{@username} in task #{short_inspect()}"
      end
      unless hs[:password].nil?
        @password = hs[:password]
        logger.info "password is update in task #{short_inspect()}"
      end
    end

    protected
    # Requests links from readitlater.
    #
    # @return [Array<Link>] an array of links
    # @raise [TaskRequestError] if error happens when getting response
    def request
      begin
        logger.info 'Start sending request in task' + short_inspect
        logger.debug inspect
        opts = {}
        opts[:since] = @since unless @since.nil?
        opts[:tags] = 1
        res = @client.request(@apikey, @username, @password, opts)
        logger.info 'Finish sending request in task' + short_inspect
        logger.debug inspect

        logger.info 'Start parsing links in task' + short_inspect
        logger.debug inspect
        links = parse(res)
        logger.info 'Finish parsing links in task' + short_inspect
        logger.debug inspect
        links
      rescue Clients::ClientRequestError => e
        raise TaskRequestError, e.to_s
      end
    end
    
    private

    # Parses links from the responses.
    #
    # @param [Net::HTTPResponse,Array<Net::HTTPResponse>] responses one or
    #   an array of http responses
    # @return [Array<Link>] an array of parsed links
    # @raise [TaskRequestError] if parse error happens
    def parse(responses)
      responses = [responses] unless responses.kind_of?(Enumerable)
      links = []
      responses.each do |res|
        parser = Yajl::Parser.new
        begin
          res_obj = parser.parse(res.body)
          next unless res_obj['status'].to_i == 1 # 1=normal, 2=no changes
          # update 'since'
          @since = res_obj['since'].to_i

          res_obj['list'].each_value do |obj|
            link = Link.new({
                :userid => userid,
                :url => obj['url'],
                :date => Time.at(obj['time_updated'].to_i),
                :extra => obj['tags'] || '',
                :extra_id_str => obj['item_id'] || '',
                :provider => Linkrawler::Clients::READITLATER
              })
            links << link
          end
        rescue Yajl::ParseError => e
          logger.error e + "in task #{short_inspect}"
          logger.error "response:\n#{res.body}"
          raise TaskRequestError, e.to_s
        end
      end
      links
    end
  end
end ; end
