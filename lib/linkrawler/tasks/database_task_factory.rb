require 'time'
require 'sequel_core'
require 'openssl'
require 'digest/sha1'
require 'base64'

module Linkrawler ; module Tasks
  # This is a task factory that creates from the Linktuned MySQL database. 
  # It maintains the relationships between task property to table columns.
  # It also configures the task with proper callbacks and provided linkstores,
  # so every task created from this factory has the same callbacks and 
  # linkstores.
  #
  # @example Create a task factory, load tasks and create tasks
  #   linkstores = []
  #   # add linkstores
  #
  #   factory = DatabaseTaskFactory.new('database', 'usern', 'password, 'host', 
  #     3306,
  #     {:twitter => {:consumer_key => 'xxx', :consumer_secret => 'yyy'},
  #      :readitlater => {:apikey => 'xxx'}
  #     },
  #     linkstores)
  #
  #   tasks = factory.loadTasksFromDatabase()
  #   task = factory.createDeliciousTask(1, 'username', 2)
  #
  class DatabaseTaskFactory
    include Log

    # table name
    TABLE = :authentications
    # column names
    CO_ID = :id
    CO_USER_ID = :user_id
    CO_PROVIDER = :provider
    CO_TOKEN = :token
    CO_SECRET = :secret
    CO_UID = :uid
    CO_USERNAME = :username
    CO_SINCE = :since

    # Creates a task factory based on Linktuned database.
    #
    # @param [String] database database name
    # @param [String] user user name
    # @param [String] password user password
    # @param [String] host database server host
    # @param [Integer] port database server port
    # @param [Hash] app_keys application keys for access some link services
    # @option app_keys [Hash] :twitter keys for a twitter application,
    #   including two keys +consumer_key+ and +consumer_secret+
    # @option app_keys [Hash] :readitlater keys for a readitlater application,
    #   including one key +apikey+
    # @param [Array<Linkstore::Base>] linkstores linkstores for each task
    def initialize(database, user, password, host, port=3306, app_keys={},
                    linkstores=[])
      @database = database
      @user = user
      @password = password
      @host = host
      @port = port
      @app_keys = app_keys
      @linkstores = linkstores || []
    end
    
    # Loads tasks from database that keeps various authentication methods for
    # each user.
    #
    # @yield [Task] a block to execute with each task
    # @return [Array<Task>] an array of various tasks
    # @raise [TaskLoadError] if load tasks failed
    def loadTasksFromDatabase
      db = DbHelper::connect_to_mysql(@database, @user, @password, @host, @port)
      if db.nil?
        logger.error 'Cannot connect to database'
        raise TaskLoadError, 'Cannot connect to database'
      end
      unless db.table_exists?(TABLE)
        logger.error "Table #{TABLE} doesn't exist"
        raise TaskLoadError, "Table #{TABLE} doesn't exist"
      end

      tasks = []
      ds = db[TABLE]
      ds.each do |row|
        task = case row[CO_PROVIDER]
          when Clients::TWITTER
            createTwitterTask(row[CO_ID], row[CO_TOKEN],row[CO_SECRET], 
                              row[CO_USERNAME], row[CO_USER_ID],
                              row[CO_SINCE])
          when Clients::FACEBOOK
            createFacebookTask(row[CO_ID], row[CO_TOKEN],row[CO_USER_ID], 
                              row[CO_SINCE])
          when Clients::DELICIOUS
            createDeliciousTask(row[CO_ID], row[CO_USERNAME], row[CO_USER_ID], 
                              row[CO_SINCE])
          when Clients::READITLATER
            createReaditlaterTask(row[CO_ID], row[CO_TOKEN], 
              decrypt(row[CO_SECRET],@app_keys[:readitlater][:encrypt][:key],@app_keys[:readitlater][:encrypt][:iv]), 
              row[CO_USER_ID], row[CO_SINCE])
          end

          yield task if block_given?
          tasks << task
      end

      DbHelper::disconnect(db) # disconnect from database
      tasks
    end

    # Creates a twitter task.
    #
    # @param [Integer] id task id
    # @param [String] token token for accessing the twitter account
    # @param [String] secret secret for accessing the twitter account
    # @param [String] username username of the twitter account
    # @param [Integer] user_id user id of the owner
    # @param [Integer] since last retrieved tweets id 
    # @param [String] consumer_key consumer key for the twitter client
    # @param [String] consumer_secret consumer secret for the twitter client
    # @return [TwitterTask] a twitter task
    def createTwitterTask(id, token, secret, username, user_id, since=nil, consumer_key=nil, consumer_secret=nil)
      consumer_key = @app_keys[:twitter][:consumer_key] if consumer_key.nil?
      consumer_secret = @app_keys[:twitter][:consumer_secret] if consumer_secret.nil?
      task = TwitterTask.new(id, consumer_key, consumer_secret,
              token, secret, username, user_id,
              since.nil? ? nil : since.to_i )
      preconfig_task(task)
    end

    # Creates a Facebook task.
    #
    # @param [Integer] id task id
    # @param [String] token token grants access permission to the user's account
    # @param [Integer] user_id user id of the owner
    # @param [Integer] since the timestamp of last request
    # @return [FacebookTask] a facebook task
    def createFacebookTask(id, token, user_id, since=nil)
      task = FacebookTask.new(id, token, user_id, 
              since.nil? ? nil : since.to_i)
      preconfig_task(task)
    end

    # Create a delicious task.
    #
    # @param [Integer] id task id
    # @param [String] username username of the delicous account
    # @param [Integer] user_id user id of the owner
    # @param [Integer] since the timestamp of last request
    # @return [DeliciousTask] a delicious task
    def createDeliciousTask(id, username, user_id, since=nil)
      task = DeliciousTask.new(id, username, user_id, 
              since.nil? ? nil : since.to_i)
      preconfig_task(task)
    end

    # Creates a readitlater task.
    #
    # @param [Integer] id task id
    # @param [String] username user's name
    # @param [String] password user's password
    # @param [Integer] user_id user id of the owner
    # @param [Integer] since the timestamp of last request
    # @param [String] apikey the app's API key
    # @return [ReaditlaterTask] a readitlater task
    def createReaditlaterTask(id, username, password, user_id, since=nil, apikey=nil)
      apikey = @app_keys[:readitlater][:apikey] if apikey.nil?
      task = ReaditlaterTask.new(id, apikey, username, password, 
              user_id, since.nil? ? nil : since.to_i)
      preconfig_task(task)
    end

    private

    def preconfig_task(task)
      # add after_save callback to links
      task.after_save do |links|
        unless task.since.nil?
          db = DbHelper::connect_to_mysql(@database,@user,@password,@host,@port)
          db[TABLE].filter(CO_ID => task.id).update(CO_SINCE => task.since.to_s)
          DbHelper::disconnect(db)
        end
      end

      # add link stores to task
      @linkstores.each do |ls|
        task.add_link_store(ls)
      end

      task
    end

    def decrypt(secret, encrypt_key, encrypt_iv)
      c = OpenSSL::Cipher::Cipher.new('aes-256-cbc')
      c.decrypt
      c.key = Digest::SHA1.hexdigest(encrypt_key)
      c.iv = encrypt_iv
      d = c.update(Base64.decode64(secret))
      d << c.final
      d
    end

  end
end ; end
