require 'yajl'
require 'time'

module Linkrawler ; module Tasks
  # A task that retrieves links from Delicious[http://delicious.com].
  #
  # @example Creates a delicious task and run it
  #
  #   task = Linkrawler::Tasks::DeliciousTask.new(1,'magic003')
  #   link_store = MockLinkStore.new
  #   task.add_link_store(link_store)
  #   task.after_save do |links|
  #     # handle the saved links
  #   end
  #   task.run
  #
  class DeliciousTask < Task
    # Creates a delicious task.
    #
    # @param [Integer] id task id
    # @param [String] username username of the delicious account
    # @param [Integer] userid user id in this application. Typically used when 
    #   saving a link
    # @param [Integer] since it could be the timestamp at which last request 
    #   is sent or an integer value of the latest retrieved object's id
    def initialize(id, username, userid, since=nil)
      super(id, userid, since)
      @username = username
      @client = Clients::DeliciousClientSingleton.instance
    end

    protected
    # Requests bookmarks from delicious.
    #
    # @return [Array<Link>] an array of links
    # @raise [TaskRequestError] if error happens when getting response
    def request
      begin
        logger.info 'Start sending request in task' + short_inspect
        logger.debug inspect
        res = @client.request(@username)
        logger.info 'Finish sending request in task' + short_inspect
        logger.debug inspect

        @since =  Time.now.to_i
        logger.info 'Start parsing links in task' + short_inspect
        logger.debug inspect
        links = parse(res)
        logger.info 'Finish parsing links in task' + short_inspect
        logger.debug inspect
        links
      rescue Clients::ClientRequestError => e
        raise TaskRequestError, e.to_s
      end
    end

    # Updates the properties of this task.
    #
    # @param [Hash] hs hash of properties to be updated
    # 
    # @option hs [Integer] :userid user id of this task
    # @option hs [Integer] :since timestamp for the last request
    # @option hs [String] :username username of the account
    #
    # @see Task#update
    def update(hs)
      super(hs)
      unless hs[:username].nil?
        @username = hs[:username]
        logger.info "username is updated to #{@username} in task #{short_inspect()}"
      end
    end
    private

    # Parses links from the responses.
    # 
    # @param [Net::HTTPResponse,Array<Net::HTTPResponse>] responses one or 
    #   an array of http responses
    # @return [Array<Link>] an array of parsed links
    # @raise [TaskRequestError] if parse error happens
    def parse(responses)
      responses = [responses] unless responses.kind_of?(Enumerable)
      links = []
      responses.each do |res|
        parser = Yajl::Parser.new
        begin
          parser.parse(res.body) do |obj_arr|
            obj_arr.each do |obj|
              extra = obj['n'] || ''
              extra += ' ' + obj['t'].join(' ') if obj['t'] && !obj['t'].empty?
              link = Link.new({
                  :userid => userid,
                  :url => obj['u'],
                  :date => obj['dt'].empty? ? Time.now : Time.parse(obj['dt']),
                  :extra => extra,
                  :provider => Linkrawler::Clients::DELICIOUS
                })
              links << link
            end
          end
        rescue Yajl::ParseError => e
          logger.error e + " in task#{short_inspect}"
          logger.error "response:\n#{res.body}"
          raise TaskRequestError, e.to_s
        end
      end
      links
    end
  end
end ; end
