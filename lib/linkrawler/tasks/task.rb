require 'uri'
require 'thread'

module Linkrawler ; module Tasks
  # The abstract task. A task implements all the work regarding to request 
  # updates from a link service, parse links from them, and save links.
  #
  # It implements a method {#run} that organizes the work flow, and it is the 
  # only method needed to be called by other classes. Subclasses should override
  # the following methods:
  # * {#request}
  # 
  # A +since+ value is kept by the task, and it can be sent along with the
  # requests, so only updates since then will be retrieved. It has a variable
  # meaning, normally it is a timestamp and sometimes it could the id for an
  # update. It is the subclass's responsibility to kept it updated.
  #
  # It doesn't have a built-in link saving feature, so external 
  # {Linkstore::Base} should be provided to save links, using {#add_link_store}.
  # Multiple stores can be provided, so a link can be saved for different 
  # purposes.
  #
  # Optionally, a task can implement after save filter:
  # * {#after_save}
  #
  # Note: both {#add_link_store} and {#after_save} should be called before 
  # {#run}
  #
  # @abstract Override this class to implement a task.
  # @see Linkstore::Base
  class Task
    include Log

    # @return [Integer] id of the task
    attr_reader :id
    # @return [Integer] id of the user in this application
    attr_reader :userid
    # @return [Integer] since value for this task 
    attr_reader :since

    # Creates a new task.
    #
    # @param [Integer] id task id in this application. Typically id in the 
    #   database
    # @param [Integer] userid user id in this application. Typically used when 
    #   saving a link
    # @param [Integer] since it could be the timestamp at which last request 
    #   is sent or an integer value of the latest retrieved object's id
    def initialize(id, userid, since=nil)
      @id = id
      @userid = userid
      @since = since
      @link_stores = []
      @after_save_callback = nil
      @legacy_links = [] # links that are not successfully saved
      @running = false
      @mutex = Mutex.new
    end
    
    # Expands the url if it is shortened using some url shorten service.
    #
    # @param [String] url url to be expanded
    # @return [String] original url or the expanded one
    def self.expand_url(url)
      uri = URI(url)
      begin
        Net::HTTP.start(uri.host, uri.port) do |http|
          uri.path = "/" if uri.path.empty?
          res = http.head(uri.path)
          if res.code.eql?('301') || res.code.eql?('302') 
            # Redirected. It is a shortened url.
            return res['Location']
          end
        end
      rescue Exception 
        # ignore the exception
      end
      return url
    end

    # Extracts URLs from the text.
    #
    # @param [String] text input text
    # @return [Array<String>] an array of extracted URLs
    def self.extract_urls(text)
      URI.extract(text,['http','https'])
    end

    # Run this task.
    # 
    # @return [void]
    def run
      logger.info 'Start running task' + short_inspect
      logger.debug inspect
      @mutex.synchronize do
        if @running
          logger.info 'Skip task because it is running ' + short_inspect
          return
        else
          @running = true
        end
      end
      begin
        links = request()
        if links.empty? || save(links)
          @after_save_callback.call(links) unless @after_save_callback.nil?
        end
      rescue TaskRequestError => e
        logger.error "Task#{short_inspect} failed: #{e.to_s}"
      end
      @mutex.synchronize do
        @running = false
      end
      logger.info 'Finish running task' + short_inspect
      logger.debug inspect
    end

    # Returns whether this is a boostrap task. A bootstrap task
    # means this is the first time requesting updates from a link service, so
    # it should request all previous updates.
    #
    # @return [Boolean] whether a boostrap task
    def bootstrap?
      @since.nil?
    end

    # Adds a link store and returns itself.
    # 
    # @param [Linkstore::Base] link_store link store
    # @return [Task] the task itself
    def add_link_store(link_store)
      @link_stores << link_store
      self
    end

    # Sets the callback that should be called after a link is saved. The 
    # callback can be sent in either as an {Proc} argument or a code block,
    # which accepts an array of {Link}s as the parameter.
    #
    # @yield [] the yielded block is called once after saving a link
    # @param [Proc] cb callback
    # @return [Task] the task itself
    def after_save(cb=nil, &block)
      @after_save_callback = cb || block
      self
    end

    # Get the short inspect string for a task
    def short_inspect
      "<id=#{@id}, user_id=#{@userid}, since=#{@since}>"
    end

    # Updates the properties of this task.
    #
    # @param [Hash] hs hash of properties to be updated
    # 
    # @option hs [Integer] :userid user id of this task
    # @option hs [Integer] :since timestamp for the last request
    def update(hs)
      unless hs[:userid].nil?
        @userid = hs[:userid]
        logger.info "userid is updated to #{@userid} for task #{short_inspect()}"
      end
      unless hs[:since].nil?
        @since = hs[:since]
        logger.info "since is updated to #{@since} for task #{short_inspect()}"
      end
    end

    protected

    # This method should be implemented to send a request and parse the 
    # response.
    #
    # 
    # @abstract
    # @return [Link,Array<Link>] one or an array of links
    # @note subclass should update 'since' in this method
    def request
      raise NotImplementedError, "#{self.class} does not support request"
    end

    # Saves the links into link stores.
    #
    # @param [Array<Model::Link>] links an array of links
    # @return [Boolean] true if all links are saved to all link stores
    def save(links)
      logger.info 'Start saving links in task' + short_inspect
      logger.debug inspect
      @link_stores ||= []
      
      legacies = []
      @link_stores.each do |ls|
        begin
          # try to save all links
          ls.save_all!(links + @legacy_links)
        rescue Linkstore::LinkstoreUnavailableError
          legacies += links
        rescue Linkstore::LinkSaveError => e
          legacies += e.failed
        end
      end
      @legacy_links = legacies.uniq
      logger.warn @legacy_links.size.to_s + " legacy links in task#{short_inspect}" unless @legacy_links.empty?
      logger.info 'Finish saving links in task' + short_inspect
      logger.debug inspect
      # true only if all links, including legacy ones, are saved
      return @legacy_links.empty?
    end
  end
end ; end
