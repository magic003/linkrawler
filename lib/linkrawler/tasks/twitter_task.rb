require 'time'
require 'yajl'

module Linkrawler ; module Tasks
  # A task that retrieves links from Twitter[http://twitter.com].
  #
  # @example Creates a twitter task and run it
  #
  #   task = Linkrawler::Tasks::TwitterTask.new("consumer_key", 
  #                                             "consumer_secret",
  #                                             "token",
  #                                             "token_secret",
  #                                             "screen_name",
  #                                             nil,
  #                                             1)
  #   link_store = MockLinkStore.new
  #   task.add_link_store(link_store)
  #   task.after_save do |links|
  #     # handle the saved links
  #   end
  #   task.run
  #
  class TwitterTask < Task
    # Creates a twitter task.
    #
    # @param [Integer] id task id
    # @param [String] consumer_key consumer key for the twitter client
    # @param [String] consumer_secret consumer secret for the twitter client
    # @param [String] token token for accessing the twitter account
    # @param [String] token_secret token secret for accessing the twitter 
    #   account
    # @param [String] screen_name screen name for the twitter account
    # @param [Integer] userid user id in this application. Typically used when
    #   saving a link
    # @param [Integer] since it could be the timestamp at which last request 
    #   is sent or an integer value of the latest retrieved object's id
    def initialize(id, consumer_key, consumer_secret, token, token_secret,
                    screen_name, userid, since=nil)
      super(id, userid,since)
      @consumer_key = consumer_key
      @consumer_secret = consumer_secret
      @token = token
      @token_secret = token_secret
      @screen_name = screen_name
      @client = Clients::TwitterClientSingleton.instance
    end

    # Updates the properties of this task.
    #
    # @param [Hash] hs hash of properties to be updated
    # 
    # @option hs [Integer] :userid user id of this task
    # @option hs [Integer] :since timestamp for the last request
    # @option hs [String] :consumer_key consumer key for the twitter client
    # @option hs [String] :consumer_secret consumer secret for the twitter client
    # @option hs [String] :token token for accessing the twitter account
    # @option hs [String] :token_secret token secret for accessing the twitter 
    #   account
    # @option hs [String] :screen_name screen name for the twitter account
    #
    # @see Task#update
    def update(hs)
      super(hs)
      unless hs[:consumer_key].nil?
        @consumer_key = hs[:consumer_key] 
        logger.info "consumer_key is updated in task #{short_inspect()}"
      end
      unless hs[:consumer_secret].nil?
        @consumer_secret = hs[:consumer_secret] 
        logger.info "consumer_secret is updated in task #{short_inspect()}"
      end
      unless hs[:token].nil?
        @token = hs[:token]
        logger.info "token is updated in task #{short_inspect()}"
      end
      unless hs[:token_secret].nil?
        @token_secret = hs[:token_secret]
        logger.info "token_secret is updated in task #{short_inspect()}"
      end
      unless hs[:screen_name].nil?
        @screen_name = hs[:screen_name]
        logger.info "screen_name is updated to #{@screen_name} in task #{short_inspect()}"
      end
    end

    protected
    # Requests tweets and parse links from twitter.
    #
    # @return [Array<Link>] an array of links
    # @raise [TaskRequestError] if error happens when getting response
    def request
      opts = {:screen_name => @screen_name}
      opts[:since_id] = @since unless @since.nil?
      opts[:count] = 200  # 200 per page
      links, page = [], 1
      old_since = @since
      while true
        opts[:page] = page
        begin
          logger.info 'Start sending request in task' + short_inspect
          logger.debug inspect
          res = @client.request(@consumer_key, @consumer_secret,
                                @token, @token_secret, opts)
          logger.info 'Finish sending request in task' + short_inspect
          logger.debug inspect
        rescue Clients::ClientRequestError => e
          raise TaskRequestError, e.to_s
        end

        # restore @since if error happens
        @since = old_since if res.nil?
        break if res.nil? || '[]'.eql?(res.body)

        logger.info 'Start parsing links in task' + short_inspect
        logger.debug inspect
        ret = parse(res)
        logger.info 'Finish parsing links in task' + short_inspect
        logger.debug inspect

        links += ret unless ret.empty?
        # ret is the parse links instead of tweets, so should not break
        # break if ret.size < opts[:count]
        page += 1
      end
      links
    end

    private

    # Parses links from the responses.
    #
    # @param [Net::HTTPResponse,Array<Net::HTTPResponse>] responses one or
    #   an array of http responses
    # @return [Array<Link>] an array of parsed links
    # @raise [TaskRequestError] if parse error happens
    def parse(responses)
      responses = [responses] unless responses.kind_of?(Enumerable)
      links = []
      responses.each do |res|
        parser = Yajl::Parser.new
        begin
          parser.parse(res.body) do |obj_arr|
            if obj_arr.instance_of?(Hash) && obj_arr['errors']
              logger.error "Error response: #{obj_arr['errors']} in task#{short_inspect}"
              next
            end

            obj_arr.each do |obj|
              unless obj['entities']['urls'].empty?
                attrs = { :userid => userid,
                          :date => Time.parse(obj['created_at']),
                          :extra => obj['text'] || '',
                          :extra_id_str => obj['id_str'] || '',
                          :provider => Linkrawler::Clients::TWITTER
                        }
                obj['entities']['urls'].each do |u|
                  attrs[:url] = u['expanded_url'] || u['url']
                  links << Link.new(attrs)
                end

                # update 'since'
                @since = obj['id_str'].to_i if @since.nil? || obj['id_str'].to_i > @since
              end
            end
          end
        rescue Yajl::ParseError => e
          logger.error e + " in task #{short_inspect}"
          logger.error "response:\n#{res.body}"
          raise TaskRequestError, e.to_s
        end
      end
      links
    end
  end
end ; end
