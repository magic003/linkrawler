module Linkrawler ; module Tasks
  # This task loader loads tasks from the Linktuned MySQL database, which keeps 
  # various authentication information for each user. It uses the
  # {DatabaseTaskFactory} to load tasks, so it does not depend on the database
  # structure anymore. 
  #
  # @example Creates a database task loader and load tasks
  #
  #   factory = DatabaseTaskFactory.new(...)
  #
  #   loader = DatabaseLoader.new(factory)
  #   tasks = loader.load
  #
  # @see TwitterTask
  # @see FacebookTask
  # @see DeliciousTask
  # @see ReaditlaterTask
  class DatabaseLoader < Loader
    include Log

    # Creates a Linktuned database task loader.
    #
    # @param [DatabaseTaskFactory] factory factory for loading tasks
    def initialize(factory)
      @factory = factory
    end

    # Loads task from database that keeps various authentication method for
    # each user.
    #
    # @return [Array<Task>] an array of various tasks
    # @raise [TaskLoadError] if load tasks failed
    def load
      tasks = @factory.loadTasksFromDatabase()
      logger.info "#{tasks.size} tasks loaded."
      tasks
    end
  end
end ; end
