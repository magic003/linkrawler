require 'uri'
require 'cgi'
require 'yajl'
require 'time'

module Linkrawler ; module Tasks
  # A task that retrieves links from Facebook[http://facebook.com].
  #
  # @example Creates a facebook task and run it
  #
  #   task = Linkrawler::Tasks::FacebookTask.new("access_token",1)
  #   link_store = MockLinkStore.new
  #   task.add_link_store(link_store)
  #   task.after_save do |links|
  #     # handle the saved links
  #   end
  #   task.run
  #
  # @note It doesn't retrieve links from inbox and outbox
  class FacebookTask < Task
    # Base batch requests
    BATCH_BASE = [
      {'method' => 'GET', 'relative_url' => 'me/links'},
#      {'method' => 'GET', 'relative_url' => 'me/outbox'},
      {'method' => 'GET', 'relative_url' => 'me/notes'},
      {'method' => 'GET', 'relative_url' => 'me/posts'},
      {'method' => 'GET', 'relative_url' => 'me/statuses'}
#      {'method' => 'GET', 'relative_url' => 'me/inbox'}
    ]

    # Limited objects for each request
    LIMIT = 200

    # Creates a facebook task.
    #
    # @param [Integer] id task id
    # @param [String] access_token token grants access permission to the user's
    #   information
    # @param [Integer] userid user id in this application. Typically used when
    #   saving a link
    # @param [Integer] since it could be the timestamp at which last request 
    #   is sent or an integer value of the latest retrieved object's id
    def initialize(id, access_token, userid, since=nil)
      super(id, userid, since)
      @access_token = access_token
      @client = Clients::FacebookClientSingleton.instance
    end

    # Updates the properties of this task.
    #
    # @param [Hash] hs hash of properties to be updated
    # 
    # @option hs [Integer] :userid user id of this task
    # @option hs [Integer] :since timestamp for the last request
    # @option hs [String] :access_token access token for Facebook
    # 
    # @see Task#update
    def update(hs)
      super(hs)
      unless hs[:access_token].nil?
        @access_token = hs[:access_token]
        logger.info "access_token is updated to #{@access_token} in task #{short_inspect()}"
      end
    end

    protected 
    # Requests objects and parse links from facebook.
    #
    # @return [Array<Link>] an array of links
    # @raise [TaskRequestError] if error happens when getting response
    def request
      batch = BATCH_BASE
      obj_types = [:links, :notes, :posts, :statuses]
      new_since = nil
      while batch.size > 0
        unless @since.nil?
          # add since parameter to each request url
          batch.each_index do |i|
            uri = URI(batch[i]['relative_url'])
            query_hash = CGI.parse(uri.query || '')
            query_hash['since'] = @since
            query_hash['limit'] = LIMIT
            uri.query = URI.encode_www_form(query_hash)
            batch[i]['relative_url'] = uri.to_s
          end
        end
        
        res = nil
        begin
          # send the request
          logger.info 'Start sending request in task' + short_inspect
          logger.debug inspect
          res = @client.request(@access_token, {:batch => batch})
          logger.info 'Finish sending request in task' + short_inspect
          logger.debug inspect
        rescue Clients::ClientRequestError => e
          raise TaskRequestError, e.to_s
        end

        # save the first successful request time as 'since'
        new_since = Time.now.to_i if new_since.nil?

        # parse the responses
        links = []
        batch_next = []
        obj_types_next = []
        parser = Yajl::Parser.new
        begin
          logger.info 'Start parsing links in task' + short_inspect
          logger.debug inspect
          obj_arr = parser.parse(res.body)
          logger.debug inspect
          if (obj_arr.instance_of? Hash) && obj_arr['error']
            logger.error obj_arr
            raise TaskRequestError, obj_arr
          end
          obj_arr.each_index do |i|
            unless obj_arr[i]['code'] == 200
              logger.error 'Errors in response:'
              logger.error obj_arr[i].to_s
              next
            end

            # if returned objects is not less than LIMIT, request again
            unless obj_arr[i]['body']['data'].size < LIMIT
              req = batch[i]
              # copy the path and queries of next page
              uri = URI(obj_arr[i]['body']['paging']['next'])
              req['relative_url'] = uri.path + '?' + uri.query
              batch_next << req
              obj_types_next << obj_types[i]
            end
            # parse links from returned objects
            parsed_links =  case obj_types[i]
                            when :links
                              parse_from_links(obj_arr[i]['body'])
                            when :notes
                              parse_from_notes(obj_arr[i]['body'])
                            when :posts
                              parse_from_posts(obj_arr[i]['body'])
                            when :statuses
                              parse_from_statuses(obj_arr[i]['body'])
                            end
            links += parsed_links
          end
          logger.info 'Finish parsing links in task' + short_inspect
        rescue Yajl::ParseError => e
          logger.error e + " in task #{short_inspect}"
          logger.error "response:\n#{res.body}"
          raise TaskRequestError, e.to_s
        end
        batch = batch_next
        obj_types = obj_types_next
      end

      # update 'since'
      @since = new_since

      links
    end

    private
    # Parses links from the links object.
    #
    # @param [String] res response text
    # @return [Array<Link>] an array of links
    def parse_from_links(res)
      parser = Yajl::Parser.new
      res_obj = parser.parse(res)
      links = []
      res_obj['data'].each do |obj|
        attrs = { :userid => userid,
                  :url => obj['link'],
                  :date => Time.parse(obj['created_time']),
                  :extra => obj['description'] || '',
                  :extra_id_str => obj['id'] || '',
                  :provider => Linkrawler::Clients::FACEBOOK
                }
        links << Link.new(attrs)
      end
      links
    end

    # Parses links from user's notes.
    #
    # @param [String] res response text
    # @return [Array<Link>] an array of links
    # @note it doesn't read the comments
    def parse_from_notes(res)
      parser = Yajl::Parser.new
      res_obj = parser.parse(res)
      links = []
      res_obj['data'].each do |obj|
        next if obj['message'].nil? # ignore stories or other things
        urls = Task.extract_urls(obj['message'])
        next if urls.empty?
        attrs = { :userid => userid,
                  :date => Time.parse(obj['updated_time'] || obj['created_time']),
                  :extra => obj['message'] || '',
                  :extra_id_str => obj['id'] || '',
                  :provider => Linkrawler::Clients::FACEBOOK
                }
        urls.each do |url|
          attrs[:url] = url
          links << Link.new(attrs)
        end
      end
      links
    end

    # Parses links from user's posts.
    #
    # @param [String] res response text
    # @return [Array<Link>] an array of links
    # @note it doesn't read the comments
    def parse_from_posts(res)
      parse_from_notes(res) # the data format is the same as notes
    end

    # Parses links from user's statuses.
    #
    # @param [String] res response text
    # @return [Array<Link>] an array of links
    # @note it doesn't read the comments
    def parse_from_statuses(res)
      parse_from_notes(res) # the data format is the same as notes
    end
  end
end ; end
