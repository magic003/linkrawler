module Linkrawler
  # The +Interface+ module contains interfaces that allow users or other 
  # applications to interact with the running linkrawler application. 
  # For example, new tasks can be added during the runtime.
  module Interface
    autoload :RackInterface,    'linkrawler/interface/rack_interface'
  end
end
