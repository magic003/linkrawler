require 'uri'
require 'net/http'
require 'singleton'

module Linkrawler ; module Clients
  # This is the singleton version of {DeliciousClient} class.
  #
  # Each task holds a client for sending the request. By using this singleton
  # client, the client object is shared by multiple tasks and a lot of memory
  # can be saved.
  #
  # @example Creates a delicious client and requests updates
  #   
  #   client = DeliciousClientSingleton.instance
  #   res = client.request('username',nil,{:format => 'json', :count => 50})
  #   puts res.body
  #
  # @see DeliciousClient
  class DeliciousClientSingleton < Base
    include Singleton

    # Base request url
    BASE_URL = 'http://feeds.delicious.com/v2/'
    # Response format
    FORMAT = 'json'
    # Max number of bookmarks
    COUNT = 100

    # Requests a list of bookmarks.
    # 
    # @param [String] username delicious username
    # @param [String] key user private key
    # @param [Hash] opts the options for the request
    # @option opts [String] :format ('json') the response format 'json' or 
    #   'rss'
    # @option opts [Integer] :count (100) number of bookmarks in 
    #   range [1..100]
    # @return [Net::HTTPResponse] an http response
    # @raise [ClientRequestError] if request failed
    def request(username,key=nil,opts={})
      format = opts[:format] || FORMAT
      count = opts[:count] || COUNT
      client = http_client
      retry_on_errors(3) do 
        client.get_response(uri(username,key,format,count))
      end
    end

    private

    def uri(username, key, format, count)
      str = BASE_URL + format + '/' + username + '?count=' + count.to_s
      # add the private key
      str += ('&private=' + key) unless key.nil?
      URI(str)
    end
  end
end ; end
