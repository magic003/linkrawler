require 'uri'
require 'net/http'
require 'yajl'
require 'singleton'

module Linkrawler ; module Clients
  # This is the singleton version of {FacebookClient} class.
  #
  # Each task holds a client for sending the request. By using this singleton
  # client, the client object is shared by multiple tasks and a lot of memory
  # can be saved.
  #
  # @example Creates a facebook client and requests objects
  #
  #   access_token = ''
  #   client = FacebookClientSingleton.instance
  #   batch = [
  #             {"method" => "GET", "relative_url" => "me/links"},
  #             {"method" => "GET", "relative_url" => "me/outbox"},
  #             {"method" => "GET", "relative_url" => "me/notes"},
  #             {"method" => "GET", "relative_url" => "me/posts"},
  #             {"method" => "GET", "relative_url" => "me/statuses"},
  #             {"method" => "GET", "relative_url" => "me/inbox"}
  #           ]
  #
  #   res = client.request(access_token,{:batch => batch})
  #   puts res.body
  #
  # @see FacebookClient
  class FacebookClientSingleton < Base
    include Singleton

    # base request url
    BASE_URL = 'https://graph.facebook.com'

    # Requests user's objects.
    #
    # @param [String] access_token token grants access permission to the user's
    # @param [Hash] opts the options for the request
    # @option opts [Array<Hash>] :batch a list of requests. Each request is a
    #   hash which contains two keys: :method and :relative_url
    # @return [Net::HTTPResponse] an http response
    # @raise [ClientRequestError] if request failed
    # @todo Currently, clients send in the batch requests(including method and
    #   urls) directly. Better to find a way so clients only specify some
    #   properties and http related stuff are encapsulated in this method.
    def request(access_token, opts={})
      uri = URI(BASE_URL)
      client = http_client
      retry_on_errors(3) do 
        client.start(uri.hostname, uri.port, :use_ssl => true) do |http|
          req = Net::HTTP::Post.new(uri.request_uri)
          req.set_form_data({'access_token' => access_token,
                             'batch' => Yajl::Encoder.encode(opts[:batch])}, '&')
          http.request(req)
        end
      end
    end
  end
end ; end
