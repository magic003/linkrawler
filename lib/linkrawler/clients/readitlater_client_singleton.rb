require 'uri'
require 'net/http'
require 'singleton'

module Linkrawler ; module Clients
  # This is the singleton version of {ReaditlaterClient} class.
  #
  # Each task holds a client for sending the request. By using this singleton
  # client, the client object is shared by multiple tasks and a lot of memory
  # can be saved.
  #
  # @example Create a readitlater client and requests links
  #
  #   client = ReaditlaterClientSingleton.instance
  #   res = client.request( 'apikey', 'username', 'password',
  #                         {:since => '1322387964',
  #                         :count => 1, 
  #                         :page => 1, 
  #                         :format => 'json'})
  #   puts res.body
  #
  # @note readitlater doesn't support OAuth yet, so need password to access
  #   the api.
  #
  # @see ReaditlaterClient
  class ReaditlaterClientSingleton < Base
    include Singleton

    # Base request url
    BASE_URL = 'https://readitlaterlist.com/v2/get'
    # Response format
    FORMAT = 'json'

    # Requests a list of links.
    #
    # @param [String] apikey this app's API key
    # @param [String] username user's name
    # @param [String] password user's password
    # @param [Hash] opts the options for the request
    # @option opts [String] :format ('json') response format, 'json' or 'xml'
    # @option opts [String] :state type of items to retrieve, 'read' or 
    #   'unread'(empty for all)
    # @option opts [Integer] :myAppOnly (0) only retrieve pages saved from this
    #   api key, 0 or 1
    # @option opts [Integer] :since only get changed/added items since this time
    # @option opts [Integer] :count number of items to retrieve, empty for all
    # @option opts [Integer] :page used with _count_ to paginate results
    # @option opts [Integer] :tags (0) retrieve tags with items, 0 or 1
    # @return [Net::HTTPResponse] an http response
    # @raise [ClientRequestError] if request failed
    def request(apikey, username, password, opts={})
      opts[:format] = FORMAT unless opts.has_key?(:format)
      opts[:apikey]= apikey
      opts[:username] = username
      opts[:password] = password
      uri = URI(BASE_URL)
      client = http_client
      res = retry_on_errors(3) do
        client.start(uri.hostname, uri.port, :use_ssl => true) do |http|
          req = Net::HTTP::Post.new(uri.request_uri)
          req.set_form_data(opts, '&');
          http.request(req)
        end
      end
      unless res.nil? || res.code.to_i == 200
        logger.error "request failed: #{res.code} #{res['X-Error']}"
        raise ClientRequestError, "request failed: #{res.code} #{res['X-Error']}"
      else
        res
      end
    end
  end
end ; end
