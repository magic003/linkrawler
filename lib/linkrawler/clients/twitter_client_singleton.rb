require 'uri'
require 'net/http'
require 'openssl'
require 'base64'
require 'singleton'

module Linkrawler ; module Clients
  # This is the singleton version of {TwitterClient} class.
  #
  # Each task holds a client for sending the request. By using this singleton
  # client, the client object is shared by multiple tasks and a lot of memory
  # can be saved.
  #
  # @example Creates a twitter client and requests user timeline
  #   
  #   consumer_key = 'XXXX'
  #   consumer_secret = 'XXXX'
  #   token = 'XXXX'
  #   token_secret = 'XXXX'
  #
  #   client = TwitterClientSingle.instance
  #   res = client.request(consumer_key, consumer_secret,token, token_secret,
  #                         {:screen_name => 'foobar',
  #                         :since_id => 134567543})
  #   puts res.body
  #   
  # @see TwitterClient
  class TwitterClientSingleton < Base
    include Singleton

    # signature method
    OAUTH_SIGNATURE_METHOD = 'HMAC-SHA1'
    # oauth version
    OAUTH_VERSION = '1.0'
    # base request url
    BASE_URL = 'https://api.twitter.com/1/statuses/user_timeline'
    # request method
    REQUEST_METHOD = 'GET'
    # default options for the query
    DEFAULT_OPTS = {
      :count => 200,
      :time_user => true,
      :include_rts => true,
      :include_entities => true,
      :exclude_replies => false,
      :contributor_details => false
    }

    # Requests the user's timeline.
    #
    # @param [String] consumer_key consumer key of this application
    # @param [String] consumer_secret consumer_secret of this application
    # @param [String] token access token for a user
    # @param [String] token_secret token secret for a user
    # @param [Hash] opts the options for the request
    # @option opts [String] :user_id id of the user 
    # @option opts [String] :screen_name screen_name of the user
    # @option opts [Integer] :since_id return results with an ID greater than
    #   the specified ID
    # @option opts [Integer] :count (200) number of tweets to return, up to 
    #   maximum of 200
    # @option opts [Integer] :max_id return results with an ID less than or 
    #   equal to the specified ID
    # @option opts [Integer] :page the page of results to retrieve
    # @option opts [Boolean] :trim_user (true) whether to include only the
    #   author's user ID
    # @option opts [Boolean] :include_rts (true) whether to include the native
    #   retweets
    # @option opts [Boolean] :include_entities (true) whether to include a 
    #   entities node for each tweet
    # @option opts [Boolean] :exclude_replies (false) whether to include replies
    # @option opts [Boolean] :contributor_details (false) whether to include
    #   the detail information for a contributor
    # @param [String] format response format, 'json', 'xml', 'rss' or 'atom'
    # @return [Net::HTTPResponse] an http response
    # @raise [ClientRequestError] if request failed
    # @note Either +:user_id+ or +screen_name+ should be specified 
    def request(consumer_key,consumer_secret,token,token_secret,
                opts={},format='json')
      url = BASE_URL + '.' + format
      # merge options
      queries = DEFAULT_OPTS.merge(opts)
      # change keys to strings
      queries = queries.inject({}) {|memo,(k,v)| memo[k.to_s] =v; memo}
      # send request
      uri = URI(url)
      uri.query = URI.encode_www_form(queries)

      client = http_client
      res = retry_on_errors(3) do 
        client.start(uri.hostname, uri.port,:use_ssl => true) do |http|
          req = Net::HTTP::Get.new(uri.request_uri)
          req['Authorization'] = oauth_header(consumer_key, consumer_secret,
                                              token, token_secret, url,
                                              queries)
          http.request(req)
        end
      end

      unless res.nil? || res.code.to_i == 200
        logger.error "request failed: #{res.code} #{res.message}"
        raise ClientRequestFailed, "request failed: #{res.code} #{res.message}"
      else
        res
      end
    end

    private
    # Encodes a string using URL form encoding.
    #
    # @param [String] string a string to be encoded
    # @return [String] encoded string
    def percent_encode(string)
      return URI.encode_www_form_component(string).gsub('*', '%2A')
    end
    
    # Generates the OAuth value for +Authentication+ header.
    #
    # @param [String] consumer_key consumer key of this application
    # @param [String] consumer_secret consumer_secret of this application
    # @param [String] token access token for a user
    # @param [String] token_secret token secret for a user
    # @param [String] url request url
    # @param [Hash] queries url queries
    # @return [String] +Authentication+ header value
    def oauth_header(consumer_key, consumer_secret, token, token_secret, url,
                      queries)
      oauth_pairs = {
        'oauth_consumer_key' => consumer_key,
        'oauth_nonce' => Array.new(5) { rand(256) }.pack('C*').unpack('H*').first,
        'oauth_signature_method' => OAUTH_SIGNATURE_METHOD,
        'oauth_timestamp' => Time.now.to_i.to_s,
        'oauth_token' => token,
        'oauth_version' => OAUTH_VERSION
      }
      oauth_pairs['oauth_signature'] = percent_encode(
                                        oauth_signature(consumer_secret,
                                          token_secret, url, 
                                          oauth_pairs,queries))
      pairs = []
      oauth_pairs.each do |key,val|
          pairs.push( "#{key.to_s}=\"#{val.to_s}\"")
      end
      'OAuth ' + pairs.join(', ')
    end
    
    # Generates the oauth signature.
    #
    # @param [String] consumer_secret consumer_secret of this application
    # @param [String] token_secret token secret for a user
    # @param [String] url request url
    # @param [Hash] oauth_pairs the 7 key/value oauth pairs, accept the
    #   signature
    # @param [Hash] queries url queries used for the request
    # @return [String] signature
    def oauth_signature(consumer_secret, token_secret, url, oauth_pairs, queries)
      queries.merge!(oauth_pairs)
      pairs = []
      queries.sort.each do |key, val|
        pairs.push("#{percent_encode(key.to_s)}=#{percent_encode(val.to_s)}")
      end
      query_string = pairs.join('&')

      base_str = [REQUEST_METHOD, percent_encode(url), 
                    percent_encode(query_string)].join('&')

      key = percent_encode(consumer_secret) + '&' + 
              percent_encode(token_secret)

      digest = OpenSSL::Digest::Digest.new('sha1')
      hmac = OpenSSL::HMAC.digest(digest, key, base_str)

      Base64.encode64(hmac).chomp.gsub(/\n/, '')
    end
  end
end ; end
