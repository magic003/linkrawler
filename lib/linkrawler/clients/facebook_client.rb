require 'uri'
require 'net/http'
require 'yajl'

module Linkrawler ; module Clients
  # This is the facebook client. It uses the 
  # batch[http://developers.facebook.com/docs/reference/api/batch] API to
  # retrieve user's links, messages, notes, posts, status messages and threads.
  #
  # @example Creates a facebook client and requests objects
  #
  #   access_token = ''
  #   client = FacebookClient.new(access_token)
  #   batch = [
  #             {"method" => "GET", "relative_url" => "me/links"},
  #             {"method" => "GET", "relative_url" => "me/outbox"},
  #             {"method" => "GET", "relative_url" => "me/notes"},
  #             {"method" => "GET", "relative_url" => "me/posts"},
  #             {"method" => "GET", "relative_url" => "me/statuses"},
  #             {"method" => "GET", "relative_url" => "me/inbox"}
  #           ]
  #
  #   res = client.request({:batch => batch})
  #   puts res.body
  #
  # @see FacebookClientSingleton
  class FacebookClient < Base
    # base request url
    BASE_URL = 'https://graph.facebook.com'

    # Creates a facebook client.
    #
    # @param [String] access_token token grants access permission to the user's
    #   information
    def initialize(access_token)
      @access_token = access_token
    end
    
    # Requests user's objects.
    #
    # @param [Hash] opts the options for the request
    # @option opts [Array<Hash>] :batch a list of requests. Each request is a
    #   hash which contains two keys: :method and :relative_url
    # @return [Net::HTTPResponse] an http response, nil if failed
    # @todo Currently, clients send in the batch requests(including method and
    #   urls) directly. Better to find a way so clients only specify some
    #   properties and http related stuff are encapsulated in this method.
    def request(opts={})
      uri = URI(BASE_URL)
      client = http_client
      retry_on_errors(3) do 
        client.start(uri.hostname, uri.port, :use_ssl => true) do |http|
          req = Net::HTTP::Post.new(uri.request_uri)
          req.set_form_data({'access_token' => @access_token,
                             'batch' => Yajl::Encoder.encode(opts[:batch])}, '&')
          http.request(req)
        end
      end
    end
  end
end ; end
