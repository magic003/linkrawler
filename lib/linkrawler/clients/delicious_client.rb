require 'uri'
require 'net/http'

module Linkrawler ; module Clients
  # This is the delicious client. It uses the 
  # feeds[http://delicious.com/help/feeds] API to retrieve bookmarks.
  #
  # Due to the API limitations, only the latest 100 bookmarks can be retrieved.
  # Any bookmark before that cannot be get.
  #
  # @example Creates a delicious client and requests updates
  #   
  #   client = DeliciousClient.new('username')
  #   res = client.request({:format => 'json', :count => 50})
  #   puts res.body
  #
  # @see DeliciousClientSingleton
  class DeliciousClient < Base
    # Base request url
    BASE_URL = 'http://feeds.delicious.com/v2/'
    # Response format
    FORMAT = 'json'
    # Max number of bookmarks
    COUNT = 100

    # Creates a delicious client.
    #
    # @param [String] username delicious username
    # @param [String] key user private key
    def initialize(username,key=nil)
      @username = username
      @key = key
    end

    # Requests a list of bookmarks.
    # 
    # @param [Hash] opts the options for the request
    # @option opts [String] :format ('json') the response format 'json' or 
    #   'rss'
    # @option opts [Integer] :count (100) number of bookmarks in 
    #   range [1..100]
    # @return [Net::HTTPResponse] an http response, nil if failed
    def request(opts={})
      format = opts[:format] || FORMAT
      count = opts[:count] || COUNT
      client = http_client
      retry_on_errors(3) do 
        client.get_response(uri(format,count))
      end
    end

    private

    def uri(format, count)
      str = BASE_URL + format + '/' + @username + '?count=' + count.to_s
      # add the private key
      str += ('&private=' + @key) unless @key.nil?
      URI(str)
    end
  end
end ; end
