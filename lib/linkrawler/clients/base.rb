require 'net/http'
require 'uri'

module Linkrawler ; module Clients
  # The abstract link service client. A client is responsible for sending
  # requests to the link service, with url query, headers(especailly the 
  # oauth +Authentication+ header) and other necessary information set 
  # appropriately. For instance, a {TwitterClient} generates the 
  # +Authentication+ header and requests for updates.
  #
  # To implement a client, override the following methods:
  # * {#request}
  #
  # @abstract Override this class to implement a link service client.
  class Base
    include Log

    # Requests updates.
    #
    # @abstract This method should implement the logic that requests updates
    #   from a specific link service.
    # @param [Hash] opts the options that will be included in http headers or
    #   queries
    # @return [Net::HTTPResponse] a http response object, nil if failed
    def request(opts={}) end

    protected
    
    # Gets a HTTP client objects. It sets proxy to the client if the
    # *http_proxy* environment variable is available.
    #
    # @return [Net::HTTP] an HTTP client object
    def http_client
      proxy_host = proxy_port = proxy_user = proxy_pass = nil
      unless ENV['http_proxy'].nil?
        uri = URI.parse(ENV['http_proxy'])
        proxy_host, proxy_port = uri.host, uri.port
        proxy_user, proxy_pass = uri.userinfo.split(/:/) unless uri.userinfo.nil?
      end
      Net::HTTP::Proxy(proxy_host, proxy_port, proxy_user, proxy_pass)
    end

    # Retries several times if error happens when sending the requests.
    #
    # @param [Integer] tries number of retries to perform
    # @yield [] code block for sending requests
    # @return [Object] return value of the code block, nil if no success
    # @raise [ClientRequestError] if request failed for 3 times
    def retry_on_errors(tries=1)
      tried = tries
      begin
        return yield
      rescue Timeout::Error, Exception => e
        if (tried -= 1) > 0
          logger.warn 'Request failed, try again in 3 seconds'
          sleep 3 # sleep for 3 seconds
          retry
        end
        logger.error "Request failed after trying #{tries} times:"
        logger.error e.to_s
        raise ClientRequestError, e.to_s
      end
    end
  end
end ; end
