require 'logger'

module Linkrawler
  # This +Log+ module provides the logger solution for this project. Any class
  # can add log function by including it. A ruby {Logger} object can be obtained
  # by calling the mixed in {#logger} method. Due to this mixin magic, the 
  # name of the container class is set on the +logger+ object, so each log 
  # messages contains the specific classname.
  #
  # == Example
  # 
  #   class Any
  #     include Log
  #
  #     def run
  #       logger.warn "About to run..."
  #     end
  #   end
  #
  # == Customization
  #
  # By default, +STDOUT+ is used as the +logdev+. Programmer can customize this
  # as well as other properties +shift_age+, +shift_size+, and +level+. 
  #
  #   # default values
  #   logdev = STDOUT
  #   shift_age = 'monthly'
  #   shift_size = 1048576
  #   level = Logger::ERROR
  #
  # Properties can be changed by:
  #
  #   # Customize
  #   Log.logdev = 'app.log'
  #   Log.shift_age = 'daily'
  #   Log.shift_size = 4096
  #   Log.level = Logger::WARN
  #
  # The above code has a global effect. If you only want to change them in a
  # particular class, you can change the object obtained from +logger+ method.
  module Log
    # Obtains a logger for the enclosed class.
    #
    # @return [Logger] logger with current class name as the +progname+
    def logger
      @logger ||= Log.logger_for(self.class.name)
    end
    
    # Use a hash to cache a unique logger per class
    @loggers = {}

    @logdev = STDOUT
    @shift_age = 'monthly'
    @shift_size = 1048576
    @level = Logger::ERROR

    class << self
      # Creates a logger for a class.
      #
      # @return [Logger]
      def logger_for(classname)
        @loggers[classname] ||= configure_logger_for(classname)
      end

      # Creates and configures a logger.
      #
      # @return [Logger]
      def configure_logger_for(classname)
        logger = Logger.new(@logdev,@shift_age,@shift_size)
        logger.level = @level
        logger.progname = classname
        logger
      end

      # Sets the log device.
      #
      # @param [String, IO] logdev it is a filename (String) or IO object 
      #   (typically STDOUT, STDERR, or and open file)
      # @return [void]
      def logdev=(logdev)
        @logdev = logdev
      end

      # Sets the log shift age.
      #
      # @param [Integer,String] age Number of old log files to keep, or
      #   frequency of rotation (+daily+, +weekly+ or +monthly+)
      # @return [void]
      def shift_age=(age)
        @shift_age = age
      end
      
      # Sets the log shift size.
      #
      # @param [Integer] size Maximum logfile size (only applies when 
      #   +shift_age+ is a number)
      # @return [void]
      def shift_size=(size)
        @shift_size = size
      end

      # Sets the log threshold.
      #
      # @param [Logger::Severity] level log threshold
      def level=(level)
        @level = level
      end
    end
  end
end
