require 'sequel_core'
require 'time'

module Linkrawler ; module Linkstore
  # The +DatabaseLinkstore+ saved links into the MySQL database of Linktuned. 
  # It is not a general database linkstore, but is specific to the database
  # used by Linktuned. It depends on the Linktuned table structure, so this 
  # class should be updated whenever the table structure is changed.
  #
  # @example Creates a database link store and save links
  #
  #   ls = DatabaseLinkstore.new('database','user','password','host',3306)
  #   links = []
  #   # add some links
  #   ls.save_all(links)
  #
  #   begin
  #     ls.save_all!(links)
  #   rescue LinkstoreUnavailableError
  #     # handle
  #   rescue LinkSaveError
  #     # handle
  #   end
  class DatabaseLinkstore < Base
    # table name
    TABLE_LINKS = :links
    TABLE_EXTRAS = :extras

    # column names
    CO_ID = :id
    CO_USER_ID = :user_id
    CO_URL = :url
    CO_DATE = :date
    CO_STATUS = :status
    CO_CREATED_AT = :created_at
    CO_UPDATED_AT = :updated_at

    # columns from table extras
    CO_EXTRA_CONTENT = :content
    CO_EXTRA_ID_STR = :id_str
    CO_PROVIDER = :provider
    CO_LINK_ID = :link_id

    # Creates a Linktuned database link store.
    #
    # @param [String] database database name
    # @param [String] user user name
    # @param [String] password user password
    # @param [String] host database server host
    # @param [Integer] port database server port
    def initialize(database, user, password, host, port=3306)
      @db = DbHelper::connect_to_mysql(database, user, password, host, port)
    end

    # Save a link to database. Returns +false+ if failed.
    # 
    # @param [Link] link the link to be saved
    # @return [Boolean] true if the link is saved successfully, otherwise false.
    # @see Base#save
    def save(link)
      return false if @db.nil? || !@db.table_exists?(TABLE_LINKS) || @db.table_exists?(TABLE_EXTRAS)
      begin
        create_or_update(link)
      rescue Sequel::Error => e
        log_error([link], e)
        return false
      end
    end
    
    # Save a link to database. Returns an exception if failed.
    # 
    # @param [Link] link the link to be saved
    # @raise [LinkstoreUnavailableError] if cannot connect to database or 
    #   table doesn't exist
    # @raise [LinkSaveError] if something wrong when saving the link
    # @see Base#save!
    def save!(link)
      raise LinkstoreUnavailableError, 'Cannot connect to database' if @db.nil?
      raise LinkstoreUnavailableError, 'Table #{TABLE_LINKS} does not exist' unless @db.table_exists?(TABLE_LINKS)
      raise LinkstoreUnavailableError, 'Table #{TABLE_EXTRAS} does not exist' unless @db.table_exists?(TABLE_EXTRAS)
      begin
        create_or_update(link)
      rescue Sequel::Error => e
        log_error([link],e)
        raise LinkSaveError.new([],[link],e.to_s)
      end
    end

    # Closes the connection to the database.
    def close
      DbHelper::disconnect(@db)
    end

    private 
    # Creates a row in the table if the link doesn't exist. Otherwise, update
    # it.
    #
    # @param [Link] link link to be saved
    def create_or_update(link)
      ds_links = @db[TABLE_LINKS]
      ds_extras = @db[TABLE_EXTRAS]
      row = ds_links.where(CO_USER_ID => link.userid, CO_URL => link.url).first
      if row.nil?  # doesn't exist
        id = ds_links.insert(CO_USER_ID => link.userid, CO_URL => link.url,
                  CO_DATE => link.date,
                  CO_CREATED_AT => Time.now,
                  CO_UPDATED_AT => Time.now)
        ds_extras.insert(CO_DATE => link.date, CO_PROVIDER => link.provider,
                  CO_EXTRA_CONTENT => link.extra, 
                  CO_EXTRA_ID_STR => link.extra_id_str,
                  CO_LINK_ID => id,
                  CO_CREATED_AT => Time.now,
                  CO_UPDATED_AT => Time.now)
      else
        id = row[CO_ID]
        extra = nil
        if Linkrawler::Clients::DELICIOUS.eql?(link.provider)
          extra = ds_extras.where(CO_DATE => link.date, CO_PROVIDER => link.provider, CO_EXTRA_CONTENT => link.extra, CO_LINK_ID => id).first
        else
          extra = ds_extras.where(CO_PROVIDER => link.provider, CO_EXTRA_ID_STR => link.extra_id_str, CO_LINK_ID => id).first
        end
        
        if extra.nil?
          # set status to reindex(4)
          row.update(CO_DATE => link.date, CO_UPDATED_AT => Time.now, CO_STATUS => 4)
          ds_extras.insert(CO_DATE => link.date, CO_PROVIDER => link.provider,
                    CO_EXTRA_CONTENT => link.extra, 
                    CO_EXTRA_ID_STR => link.extra_id_str,
                    CO_LINK_ID => id,
                    CO_CREATED_AT => Time.now,
                    CO_UPDATED_AT => Time.now)
        end
      end
    end
  end
end ; end
