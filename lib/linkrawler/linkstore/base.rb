module Linkrawler ; module Linkstore
  # The abstract link store. A link store is where links are saved. 
  # For example, a {DatabaseLinkstore} saves links to a database, and 
  # a {LoggerLinkstore} outputs links as log records.
  #
  # To implement a link store, override the following methods:
  # * {#save}
  # * {#save!}
  #
  # The other two methods {#save_all} and {#save_all!} save a list of {Link}s 
  # by calling {#save} and {#save!}. The bang! versions raise an exception if
  # there is something wrong when saving links, while the others just return
  # +false+.
  #
  # @abstract Override this class to implement a link store.
  class Base
    include Log
    # Saves a list of links.
    #
    # A retry machenism is implemented in this method. It will try 3 times
    # to save a link. If failed, it logs the error and returns +false+.
    #
    # Subclass can override it if prefer a better retry machenism.
    #
    # @param [Array<Link>] links links to be saved
    # @return [Boolean] true if all links are saved successfully, 
    #   otherwise false.
    def save_all(links)
      tried = 1
      failed = []
      until links.empty? || tried > 3
        failed = []
        links.each do |l|
          unless save(l)
            failed << l
          end
        end
        links = failed
        tried += 1
      end
      log_error(failed)
      return failed.empty?
    end

    # This method should be implemented to save a link.
    # 
    # @abstract
    # @param [Link] link the link to be saved
    # @return [Boolean] true if the link is saved successfully, otherwise false.
    def save(link)
      raise NotImplementedError, "#{self.class} does not support save"
    end

    # Saves a list of links.
    #
    # A retry machenism is implemented in this method. It will try 3 times
    # to save a link. If failed, it logs the error and raises an exception.
    #
    # Subclass can override it if prefer a better retry machenism.
    #
    # @param [Array<Link>] links links to be saved
    # @raise [LinkstoreUnavailableError] if the link store is not available
    # @raise [LinkSaveError] if some links are failed to be saved
    def save_all!(links)
      links_copy = links.dup
      tried = 1
      failed = []
      until links.empty? || tried > 3
        failed = []
        links.each do |l|
          begin 
            save!(l)
          rescue LinkstoreUnavailableError => lue
            # if the link store is not available, raise the exception
            raise lue
          rescue LinkSaveError
            failed << l
          end
        end
        links = failed
        tried += 1
      end
      log_error(failed)
      # raise exception if there are failed links
      unless failed.empty?
        raise LinkSaveError.new(links_copy-failed,failed)
      end
    end

    # This method should be implemented to save a link. 
    #
    # A {LinkstoreUnavailableError} should be raised if the link store cannot
    # be accessed. A {LinkSaveError} should be raised if there is something 
    # wrong when saving the link.
    #
    # @abstract
    # @param [Link] link the link to be saved
    def save!(link)
      raise NotImplementedError, "#{self.class} does not support save!"
    end

    protected
    # Logs the error for not saved links.
    #
    # @param [Array<Link>] links links failed to be saved
    # @param [Exception] exception exception happened
    def log_error(links, exception=nil)
      links.each do |l|
        logger.error "Failed to save link: #{l.to_s}"
        unless exception.nil?
          logger.error exception.to_s
          logger.error exception.backtrace.join("\n")
        end
      end
    end
  end
end ; end
