require 'uri'
require 'net/http'

module Linkrawler ; module Linkstore
  # The +LinkruncherLinkstore+ send links to the linkruncher server via HTTP
  # requests. The {#save_all} and {#save_all!} methods are overridden to send
  # a set of links in a single request.
  #
  # @exmaple Create a linkruncher link store and send links
  #
  class LinkruncherLinkstore < Base
    # Creates a Linkruncher link store.
    #
    # @param [String] url linkruncher server url
    def initialize(url)
      @url = url
    end

    # Saves a list of links.
    #
    # @param [Array<Link>] links links to be saved
    # @return [Boolean] true if all links are saved successfully, 
    #   otherwise false.
    # 
    # @see Base#save_all
    def save_all(links)
      tried = 1
      json = links_to_json(links)
      until tried > 3
        begin
          res = send_request(json)
          if res.code == 200
            return true
          end
        rescue Errno::ECONNREFUSED, Errno::EHOSTUNREACH => e
          if tried == 3
            logger.error e
          end
        end
        tried += 1
      end
      log_error(links)
      return false
    end
  
    # Saves a list of links.
    #
    # @param [Array<Link>] links links to be saved
    # @raise [LinkstoreUnavailableError] if the link store is not availabe
    # @raise [LinkSaveError] if some links are failed to be saved
    #
    # @see Base#save_all!
    def save_all!(links)
      tried = 1
      json = links_to_json(links)
      exception = nil
      until tried > 3
        begin
          res = send_request(json)
          return if res.code.to_i == 200
          if res.code.to_i == 404
            exception = LinkstoreUnavailableError.new('Linkruncher service is not available')
          else
            exception = LinkSaveError.new([],links, "Response code: #{res.code}")
          end
        rescue Errno::ECONNREFUSED, Errno::EHOSTUNREACH => e
          exception = LinkstoreUnavailableError.new("Cannot connect to Linkruncher: #{e.to_s}")
        end
        tried += 1
      end
      log_error(links)
      raise exception
    end

    # Sends a link to Linkruncher server. Returns +false+ if failed.
    # 
    # @param [Link] link the link to be saved
    # @return [Boolean] true if the link is saved successfully, otherwise false.
    # @see Base#save
    def save(link)
      save_all([link])
    end

    # Sends a link to Linkruncher server. Raises an exception if failed.
    #
    # @param [Link] link the link to be saved
    # @raise [LinkstoreUnavailableError] if Linkruncher server is not available
    # @raise [LinkSaveError] if something wrong when saving the link
    # @see Base#save!
    def save!(link)
      save_all!([link])
    end

    private

    # Sends the POST request to linkruncher.
    #
    # @param [String] json the JSON string to send
    # @return [Net::HTTPResponse] response
    def send_request(json)
      uri = URI(@url)
      Net::HTTP.start(uri.hostname, uri.port) do |http|
        req = Net::HTTP::Post.new(uri.request_uri)
        req.set_form_data({'links' => json},'&')
        http.request(req)
      end
    end
    
    # Generate the json string for a list of links.
    #
    # @param [Link,Array<Link>] links a list of links
    # @return [String] json for the links
    def links_to_json(links)
      links = [links] unless links.kind_of?(Enumerable)
      json = '['
      links.each do |l|
        json << "{\"url\":\"#{l.url}\",\"uid\":\"#{l.userid}\"},"
      end
      json.chomp!(',')
      json << ']'
      json
    end
  end
end ; end
