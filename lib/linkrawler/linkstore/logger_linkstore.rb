module Linkrawler ; module Linkstore
  # This is a link store that outputs a link as a log record. A {Logger} object
  # should be provided. Otherwise, a {LinkstoreUnavailableError} exception will
  # be raised when saving links.
  #
  # @example  Creates a logger link store and save links
  #   
  #   logger = Logger.new(STDOUT)
  #   ls = LoggerLinkstore.new(logger)
  #   links = [] 
  #   # add links
  #   ls.save_all(links)
  #
  #   begin
  #     ls.save_all!(links)
  #   rescue LinkstoreUnavailableError
  #     # handle
  #   rescue LinkSaveError
  #     # handle
  #   end
  #
  # @note It uses the +INFO+ level to log links, so make sure the logger
  #   provided has a level higher than +INFO+.
  class LoggerLinkstore < Base
    # Creates a logger link store.
    #
    # @param [Logger] logger a logger to be used
    def initialize(logger)
      @link_logger = logger
    end

    # Outputs a link. Returns +false+ if failed to log.
    # 
    # @param [Link] link the link to be saved
    # @return [Boolean] true if the link is saved successfully, otherwise false.
    # @see Base#save
    def save(link)
      return false if @link_logger.nil?
      begin
        @link_logger.info link.to_s
      rescue StandardError => e
        log_error([link],e)
        return false
      end
    end

    # Outputs a link. Raises an exception if failed to log.
    # 
    # @param [Link] link the link to be saved
    # @raise [LinkstoreUnavailableError] if logger is nil
    # @raise [LinkSaveError] if something wrong when saving the link
    # @see Base#save!
    def save!(link)
      raise LinkstoreUnavailableError, 'logger is nil' if @link_logger.nil?
      begin
        @link_logger.info link.to_s
      rescue StandardError => e
        log_error([link],e)
        raise LinkSaveError.new([],[link],e.to_s)
      end
    end
  end
end ; end
