module Linkrawler
  # The +ThreadPool+ module contains some implementations for the thread pools.
  module ThreadPool
    # This is the exception raised when there is no idle thread exists.
    class NoIdleThreadError < StandardError ; end

    autoload :FixedThreadPool,  'linkrawler/thread_pool/fixed_thread_pool'
  end
end
