require 'rack'
require 'yajl'

module Linkrawler ; module Interface
  # This class provides a Restful api for the running linkrawler application. 
  # It obeys the Rack[http://rack.rubyforge.org/doc/SPEC.html] specification,
  # which makes it be deployed in various web servers very easily.
  #
  # == It provides the following APIs:
  # === Add a task 
  # * Request
  #   * method: POST
  #   * body: a task in JSON format, see Data format section. 
  #
  # * Response
  #   * status: 200(OK) or 400(Bad request)
  #   * body: N/A
  #
  # == Data format
  # === Task
  # * twitter: 
  #     {
  #       "type" : "twitter",
  #       "task" : {"consumer_key" : "", "consumer_secret" : "", "token" : "", "token_secret" : "", "screen_name" : "", "userid" : 1, "since" : "123456789"}
  #     }
  # * facebook: 
  #     {
  #       "type" : "facebook",
  #       "task" : {"access_token" : "", "userid" : 1, "since" : "23422323"}
  #     }
  # * readitlater:
  #     {
  #       "type" : "readitlater",
  #       "task" : {"apikey" : "", "username" : "", "password" : "", "userid" : 1, "since" => "234223232"}
  #     }
  # * delicious:
  #     {
  #       "type" : "delicious",
  #       "task" : {"username" : "", "userid" : 1, "since" => "1233434343" }
  #     }
  #
  # @example Runs this rack interface in a web server
  #
  class RackInterface
    include Log

    # Creates a rack interface application.
    #
    # @param [Dispatcher::Base] dispatcher dispatcher used in linkrawler 
    #   application
    # @param [Tasks::DatabaseTaskFactory] factory factory for creating tasks
    #
    def initialize(dispatcher,factory)
      @dispatcher = dispatcher
      @factory = factory
    end

    # The Rack specification method.
    #
    # @param [Hash] env request environment
    # @return [[Integer,Hash,String]] an array [Status, Headers, Body]
    def call(env)
      req = Rack::Request.new(env)
      if req.post?
        handle_post(req)
      elsif req.put?
        handle_put(req)
      else
        logger.error "Unsupported request method #{req.request_method}"
        [400, {}, StringIO.new("Unsupported request method #{req.request_method}")]
      end
    end

    private 

    # Raises an {Yajl::ParseError} for missing required fields of a task.
    #
    # @raise [Yajl::ParseError] if missing required fields
    def raise_field_missing_exception
      raise Yajl::ParseError, "Some required fields are missing"
    end

    # Handles the post request.
    #
    # @param [Rack::Request] req the request object
    # @return [[Integer,Hash,String]] an array [Status, Headers, Body]
    def handle_post(req)
      body = req.body.read.to_s
      parser = Yajl::Parser.new
      begin 
        parser.parse(body) do |obj|
          task_obj = obj['task']
          task = case obj['type']
            when Clients::TWITTER
              if task_obj['id'].nil? ||
                task_obj['consumer_key'].nil? || 
                task_obj['consumer_secret'].nil? ||
                task_obj['token'].nil? ||
                task_obj['token_secret'].nil? ||
                task_obj['screen_name'].nil? ||
                task_obj['userid'].nil?
                raise_field_missing_exception
              end
                
              @factory.createTwitterTask(task_obj['id'],
                task_obj['token'],task_obj['token_secret'],
                task_obj['screen_name'], task_obj['userid'],
                task_obj['since'])
            when Clients::FACEBOOK
              if task_obj['id'].nil? ||
                task_obj['access_token'].nil? ||
                task_obj['userid'].nil?
                raise_field_missing_exception
              end
              @factory.createFacebookTask(task_obj['id'],
                task_obj['access_token'],
                task_obj['userid'],
                task_obj['since']
              )
            when Clients::READITLATER
              if task_obj['id'].nil? || 
                task_obj['apikey'].nil? ||
                task_obj['username'].nil? ||
                task_obj['password'].nil? ||
                task_obj['userid'].nil?
                raise_field_missing_exception
              end
              @factory.createReaditlaterTask(task_obj['id'],
                task_obj['username'], task_obj['password'], 
                task_obj['userid'],
                task_obj['since']
              )
            when Clients::DELICIOUS
              if task_obj['id'].nil? ||
                task_obj['username'].nil? ||
                task_obj['userid'].nil?
                raise_field_missing_exception
              end
              @factory.createDeliciousTask(task_obj['id'],
                task_obj['username'], 
                task_obj['userid'],
                task_obj['since']
              )
            else
              raise Yajl::ParseError, "Not recognized task #{obj['type']}"
            end

          @dispatcher << task
          logger.info "Task added from REST API: #{task.short_inspect}"
          return [200, {}, StringIO.new('')]
        end
      rescue Yajl::ParseError => e
        logger.error e.to_s
        logger.error "request body: #{body}"
        [400, {}, StringIO.new('Incorrect JSON format')]
      end
    end

    # Handles the put request.
    #
    # @param [Rack::Request] req the request object
    # @return [[Integer,Hash,String]] an array [Status, Headers, Body]
    def handle_put(req)
      body = req.body.read.to_s
      parser = Yajl::Parser.new
      begin 
        parser.parse(body) do |obj|
          task_obj = obj['task']
          if task_obj['id'].nil?
            raise_field_missing_exception
          end
          task_hash = {}
          case obj['type']
            when Clients::TWITTER
              task_hash[:consumer_key] = task_obj['consumer_key'] unless task_obj['consumer_key'].nil?
              task_hash[:consumer_secret] = task_obj['consumer_secret'] unless task_obj['consumer_secret'].nil?
              task_hash[:token] = task_obj['token'] unless task_obj['token'].nil?
              task_hash[:token_secret] = task_obj['token_secret'] unless task_obj['token_secret'].nil?
              task_hash[:screen_name] = task_obj['screen_name'] unless task_obj['screen_name'].nil?
              task_hash[:userid] = task_obj['userid'] unless task_obj['userid'].nil?
              task_hash[:since] = task_obj['since'] unless task_obj['since'].nil?
            when Clients::FACEBOOK
              task_hash[:access_token] = task_obj['access_token'] unless task_obj['access_token'].nil?
              task_hash[:userid] = task_obj['userid'] unless task_obj['userid'].nil?
              task_hash[:since] = task_obj['since'] unless task_obj['since'].nil?
            when Clients::READITLATER
              task_hash[:apikey] = task_obj['apikey'] unless task_obj['apikey'].nil?
              task_hash[:username] = task_obj['username'] unless task_obj['username'].nil?
              task_hash[:password] = task_obj['password'] unless task_obj['password'].nil?
              task_hash[:userid] = task_obj['userid'] unless task_obj['userid'].nil?
              task_hash[:since] = task_obj['since'] unless task_obj['since'].nil?
            when Clients::DELICIOUS
              task_hash[:username] = task_obj['username'] unless task_obj['username'].nil?
              task_hash[:userid] = task_obj['userid'] unless task_obj['userid'].nil?
              task_hash[:since] = task_obj['since'] unless task_obj['since'].nil?
            else
              raise Yajl::ParseError, "Not recognized task #{obj['type']}"
            end

          task = @dispatcher.update_task(task_obj['id'], task_hash)
          if task.nil?
            logger.error "Task #{task_obj} is not found."
            return [404, {}, StringIO.new('Task not found.')]
          else
            logger.info "Task updated from REST API: #{task.short_inspect()}"
            return [200, {}, StringIO.new('')]
          end
        end
      rescue Yajl::ParseError => e
        logger.error e.to_s
        logger.error "request body: #{body}"
        [400, {}, StringIO.new('Incorrect JSON format')]
      end
    end
  end
end ; end
