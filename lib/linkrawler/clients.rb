require 'linkrawler/clients/base'

module Linkrawler
  # The +Clients+ module contains the HTTP clients that request updates from 
  # link services using the developer API.
  module Clients
    # Supported clients
    TWITTER = 'twitter'
    FACEBOOK = 'facebook'
    DELICIOUS = 'delicious'
    READITLATER = 'readitlater'

    # This is the exception railsed when request failed.
    class ClientRequestError < StandardError ; end

    autoload :DeliciousClient,    'linkrawler/clients/delicious_client'
    autoload :DeliciousClientSingleton,    'linkrawler/clients/delicious_client_singleton'
    autoload :TwitterClient,      'linkrawler/clients/twitter_client'
    autoload :TwitterClientSingleton,      'linkrawler/clients/twitter_client_singleton'
    autoload :FacebookClient,     'linkrawler/clients/facebook_client'
    autoload :FacebookClientSingleton,     'linkrawler/clients/facebook_client_singleton'
    autoload :ReaditlaterClient,  'linkrawler/clients/readitlater_client'
    autoload :ReaditlaterClientSingleton,  'linkrawler/clients/readitlater_client_singleton'
  end
end
