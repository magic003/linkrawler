module Linkrawler
  # The +link+ class respresents a basic link entity that is parsed from 
  # responses and to be saved in various link stores. It keeps all the 
  # information for a link that this application is interested in.
  class Link
    # @return [Integer] user id which this link belongs to
    attr_accessor :userid
    # @return [String] url of this link
    attr_accessor :url
    # @return [Time] when this link is added
    attr_accessor :date
    # @return [String] extra text for this link    
    attr_accessor :extra
    # @return [String] id of the extra information in string type
    attr_accessor :extra_id_str
    # @return [String] link service that provides this link
    attr_accessor :provider

    # Creates a link.
    #
    # @param [Hash] attrs the attributes for a link
    # @option attrs [Integer] :userid user id which this link belongs to
    # @option attrs [String] :url url of this link
    # @option attrs [Time] :date when this link is added
    # @option attrs [String] :extra extrac text for this link. It can be used 
    #   for various purpose, such the tweet including this link, user comments 
    #   and etc.
    # @option attrs [String] :extra_id_str id of extra in string format
    # @option attrs [String] :provider link service that provides this link
    def initialize(attrs)
      @userid = attrs[:userid]
      @url = attrs[:url]
      @date = attrs[:date]
      @extra = attrs[:extra]
      @extra_id_str = attrs[:extra_id_str]
      @provider = attrs[:provider]
    end

    # Returns the link as a human-readable string.
    #
    # @return [String] a human-readable string for this link
    def to_s
      "{ 'userid': #{@userid}, 'url': '#{@url}', 'date': '#{@date}', " +
        "'extra': '#{@extra}', 'extra_id_str': '#{@extra_id_str}', " +
        "'provider': '#{@provider}' }"
    end
  end
end
