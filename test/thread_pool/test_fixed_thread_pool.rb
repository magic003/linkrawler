require File.expand_path(File.dirname(__FILE__)) + '/../test_helper'

class TestFixedThreadPool < Test::Unit::TestCase
  def test_execute
    tp = Linkrawler::ThreadPool::FixedThreadPool.new(2)

    a = 0
    tp.execute do
      a += 100
    end
    sleep 0.01
    assert_equal 100, a, "a should be updated"

    a = 1
    b = 2
    c = 3
    tp.execute do
      sleep 2
      assert_equal 3, c, "c should be blocked"
      a = 100
    end

    tp.execute do
      sleep 2
      b = 200
    end
    
    tp.execute do
      c = 300
    end

    sleep 3
    assert_equal 100,a,"a should be updated"
    assert_equal 200,b,"b should be updated"
    assert_equal 300,c,"c should be updated"
  end

  def test_execute!
    tp = Linkrawler::ThreadPool::FixedThreadPool.new(2)

    a = 0
    tp.execute! do
      a += 100
    end
    sleep 0.01
    assert_equal 100, a, "a should be updated"

    a = 1
    b = 2
    c = 3
    assert_nothing_raised Linkrawler::ThreadPool::NoIdleThreadError do
      tp.execute! do
        sleep 2
        a = 100
      end
    end

    assert_nothing_raised Linkrawler::ThreadPool::NoIdleThreadError do
      tp.execute! do
        sleep 2
        b = 200
      end
    end
    
    assert_raise Linkrawler::ThreadPool::NoIdleThreadError do
      tp.execute! do
        c = 300
      end
    end

    sleep 3
    assert_equal 100,a,"a should be updated"
    assert_equal 200,b,"b should be updated"
    assert_equal 3,c,"c should be updated"

  end
end
