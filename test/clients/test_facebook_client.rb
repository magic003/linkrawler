require File.expand_path(File.dirname(__FILE__)) + '/../test_helper'

class TestFacebookClient < Test::Unit::TestCase
  def setup
    @access_token = ''
  end

  def test_request
    client = Linkrawler::Clients::FacebookClient.new(@access_token)
    batch = [
              {"method" => "GET", "relative_url" => "me/links"},
              {"method" => "GET", "relative_url" => "me/outbox"},
              {"method" => "GET", "relative_url" => "me/notes"},
              {"method" => "GET", "relative_url" => "me/posts"},
              {"method" => "GET", "relative_url" => "me/statuses"},
              {"method" => "GET", "relative_url" => "me/inbox"}
            ]
    res = client.request({:batch => batch})
    assert_nothing_raised do
      res.value unless res.nil?
    end
  end

  def test_request_singleton
    client = Linkrawler::Clients::FacebookClientSingleton.instance
    batch = [
              {"method" => "GET", "relative_url" => "me/links"},
              {"method" => "GET", "relative_url" => "me/outbox"},
              {"method" => "GET", "relative_url" => "me/notes"},
              {"method" => "GET", "relative_url" => "me/posts"},
              {"method" => "GET", "relative_url" => "me/statuses"},
              {"method" => "GET", "relative_url" => "me/inbox"}
            ]
    res = client.request(@access_token, {:batch => batch})
    assert_nothing_raised do
      res.value unless res.nil?
    end
  end
end
