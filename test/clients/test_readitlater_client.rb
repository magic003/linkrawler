require File.expand_path(File.dirname(__FILE__)) + '/../test_helper'

class TestReaditlaterClient < Test::Unit::TestCase
  def setup
    @apikey = ''
    @username = ''
    @password = ''
  end

  def test_request
    client = Linkrawler::Clients::ReaditlaterClient.new(@apikey,
                                                        @username,
                                                        @password)
    res = client.request({:since => '1322387964',
                          :count => 10,
                          :page => 1,
                          :format => 'json'})
    assert_nothing_raised do
      res.value unless res.nil?
    end


    client = Linkrawler::Clients::ReaditlaterClient.new(@apikey,
                                                        @username,
                                                        'wrongpass')
    res = client.request({:since => '1322387964',
                          :count => 10,
                          :page => 1,
                          :format => 'json'})
    assert res.nil?, 'response should be nil if password is wrong'
  end

  def test_request_singleton
    client = Linkrawler::Clients::ReaditlaterClientSingleton.instance

    res = client.request(@apikey, @username, @password,
                          {:since => '1322387964',
                          :count => 10,
                          :page => 1,
                          :format => 'json'})
    assert_nothing_raised do
      res.value unless res.nil?
    end

    client2 = Linkrawler::Clients::ReaditlaterClientSingleton.instance
    assert_equal client, client2, 'Should be the same instance'

    res = client2.request(@apikey, @username, 'wrongpass',
                          {:since => '1322387964',
                          :count => 10,
                          :page => 1,
                          :format => 'json'})
    assert res.nil?, 'response should be nil if password is wrong'
  end
end
