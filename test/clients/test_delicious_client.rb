require File.expand_path(File.dirname(__FILE__)) + '/../test_helper'

class TestDeliciousClient < Test::Unit::TestCase
  def test_request
    client = Linkrawler::Clients::DeliciousClient.new('magic003')
    res = client.request()
    assert_nothing_raised do
      res.value unless res.nil?
    end
  end

  def test_request_singleton
    client = Linkrawler::Clients::DeliciousClientSingleton.instance
    res = client.request('magic003')
    assert_nothing_raised do
      res.value unless res.nil?
    end
  end
end
