require File.expand_path(File.dirname(__FILE__)) + '/../test_helper'

class TestTwitterClient < Test::Unit::TestCase
  def setup
    @consumer_key = ''
    @consumer_secret = ''
    @token = ''
    @token_secret = ''
    @screen_name = ''
  end

  def test_request
    client = Linkrawler::Clients::TwitterClient.new(@consumer_key, 
                  @consumer_secret,@token, @token_secret)
    res = client.request({:screen_name => @screen_name,
                          :count => 2})
    assert_nothing_raised do
      res.value unless res.nil?
    end
  end

  def test_request_singleton
    client = Linkrawler::Clients::TwitterClientSingleton.instance
    res = client.request(@consumer_key,@consumer_secret,@token,@token_secret,
                        {:screen_name => @screen_name,
                          :count => 2})
    assert_nothing_raised do
      res.value unless res.nil?
    end
  end
end
