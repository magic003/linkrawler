require File.expand_path(File.dirname(__FILE__)) + '/test_helper'

class TestDbHelper < Test::Unit::TestCase
  def setup
    @database = TestDbInfo::DATABASE
    @host = TestDbInfo::HOST
    @port = TestDbInfo::PORT
    @user = TestDbInfo::USER
    @password = TestDbInfo::PASSWORD
  end

  def test_connect_to_mysql
    assert Linkrawler::DbHelper::connect_to_mysql(@database, @user, @password, @host, @port), 'Should connect to MySQL database'
  end

  def test_disconnect
    db = Linkrawler::DbHelper::connect_to_mysql(@database, @user, @password, @host, @port)
    assert_nothing_raised do
      Linkrawler::DbHelper::disconnect(db)
    end

    assert_nothing_raised do
      Linkrawler::DbHelper::disconnect(nil)
    end
  end
end
