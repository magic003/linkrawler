require File.expand_path(File.dirname(__FILE__)) + '/../test_helper'
require 'rack/test'

class TestRackInterface < Test::Unit::TestCase
  include Rack::Test::Methods

  def app
    @dispatcher = Linkrawler::Dispatcher::PriorityDispatcher.new(30*60)
    Linkrawler::Interface::RackInterface.new(@dispatcher)
  end

  def test_add_task
    get '/'
    assert_equal 400, last_response.status, "Should not support GET method"

    post '/', 'data' => 'not a json'
    assert_equal 400, last_response.status, "Request should be JSON format"

    post '/', '{"type" : "twitter", "task" : {"access_token": "xxx"}}', {"CONTENT_TYPE" => 'application/json'}
    assert_equal 400, last_response.status, "JSON format should be correct"

    post '/', '{"type" : "readlater", "task" : {"access_token": "xxx"}}', {"CONTENT_TYPE" => 'application/json'}
    assert_equal 400, last_response.status, "Task type should be correct"

    post '/', '{"type" : "facebook", "task" : {"access_token": "xxx", "userid" : 1}}', {"CONTENT_TYPE" => 'application/json'}
    assert last_response.ok?, "Task should be added"
    assert_equal 1, @dispatcher.high_q.size, "Task should be added to dispatcher"
  end
end

