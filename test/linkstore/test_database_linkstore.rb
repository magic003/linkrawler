require File.expand_path(File.dirname(__FILE__)) + '/../test_helper'
require 'sequel_core'

class TestDatabaseLinkStore < Test::Unit::TestCase
  include Linkrawler::Linkstore

  def setup
    @database = TestDbInfo::DATABASE
    @host = TestDbInfo::HOST
    @port = TestDbInfo::PORT
    @user = TestDbInfo::USER
    @password = TestDbInfo::PASSWORD
    @db = Sequel.mysql2(:database => @database, :host => @host,
      :port => @port, :user => @user, :password => @password)
    @db.create_table DatabaseLinkstore::TABLE do
      primary_key DatabaseLinkstore::CO_ID
      Integer DatabaseLinkstore::CO_USER_ID, :null => false
      String DatabaseLinkstore::CO_URL, :null => false
      DateTime DatabaseLinkstore::CO_DATE
      String DatabaseLinkstore::CO_EXTRA
      String DatabaseLinkstore::CO_EXTRA_ID_STR
      String DatabaseLinkstore::CO_PROVIDER, :null => false
    end

    @links = [
        Linkrawler::Link.new({:userid => 10, :url => 'http://a.com', :date => Time.now, :extra => 'a.com extra', :extra_id_str => '12345678', :provider => 'twitter'}),
        Linkrawler::Link.new({:userid => 11, :url => 'http://b.com', :extra => 'b.com extra', :extra_id_str => '12345680', :provider => 'facebook'}),
        Linkrawler::Link.new({:userid => 12, :url => 'http://c.com', :date => Time.now, :extra_id_str => '12345681', :provider => 'delicious'}),
        Linkrawler::Link.new({:userid => 13, :url => 'http://d.com', :date => Time.now, :extra => 'd.com extra', :provider => 'readitlater'})
      ]
  end

  def teardown
    @db.drop_table DatabaseLinkstore::TABLE if @db.table_exists?(DatabaseLinkstore::TABLE)
    @db.disconnect
  end

  def test_save_all
    ls = DatabaseLinkstore.new(@database,@user,'wrongpass',@host,@port)
    assert !ls.save_all(@links), "Links should not be saved if database is not connected"
    ls.close

    ls = DatabaseLinkstore.new(@database,@user,@password,@host,@port)
    assert ls.save_all(@links), "Links should be saved"
    assert_equal @links.size, @db[DatabaseLinkstore::TABLE].count, "All links should be saved"

    ls.save_all(@links)
    assert_equal @links.size, @db[DatabaseLinkstore::TABLE].count, "Links should only be updated"

    assert !ls.save_all([Linkrawler::Link.new({:url => 'http://d%.com', :date => Time.now, :extra => 'd.com extra'})]), "Incomplete link should not be saved"
    assert_equal @links.size, @db[DatabaseLinkstore::TABLE].count, "Imcomplete link should not be saved"

    teardown
    assert !ls.save_all(@links), "Links should not be saved if table does not exist"

    ls.close
  end

  def test_save_all!
    ls = DatabaseLinkstore.new(@database,@user,'wrongpass',@host,@port)
    assert_raise LinkstoreUnavailableError, "Links should not be saved if database is not connected" do 
      ls.save_all!(@links)
    end
    ls.close

    ls = DatabaseLinkstore.new(@database,@user,@password,@host,@port)
    assert_nothing_raised "Links should be saved" do
      ls.save_all!(@links)
    end
    assert_equal @links.size, @db[DatabaseLinkstore::TABLE].count, "All links should be saved"

    ls.save_all!(@links)
    assert_equal @links.size, @db[DatabaseLinkstore::TABLE].count, "Links should only be updated"

    assert_raise LinkSaveError, "Incomplete link should not be saved" do
      ls.save_all!([Linkrawler::Link.new({:url => 'http://d%.com', :date => Time.now, :extra => 'd.com extra'})])
    end
    assert_equal @links.size, @db[DatabaseLinkstore::TABLE].count, "Imcomplete link should not be saved"

    teardown
    assert_raise LinkstoreUnavailableError, "Links should not be saved if table does not exist" do
     ls.save_all!(@links)
    end

    ls.close
  end
end
