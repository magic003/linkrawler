require File.expand_path(File.dirname(__FILE__)) + '/../test_helper'
require 'time'

class TestLoggerLinkstore < Test::Unit::TestCase
  def setup
    @links = [
        Linkrawler::Link.new({:url => 'http://a.com', :date => Time.now, :extra => 'a.com extra', :extra_id_str => '12345678'}),
        Linkrawler::Link.new({:userid => 10, :date => Time.now, :extra => 'extra', :extra_id_str => '12345679'}),
        Linkrawler::Link.new({:userid => 11, :url => 'http://b.com', :extra => 'b.com extra', :extra_id_str => '12345680'}),
        Linkrawler::Link.new({:userid => 12, :url => 'http://c.com', :date => Time.now, :extra_id_str => '12345681'}),
        Linkrawler::Link.new({:userid => 13, :url => 'http://d.com', :date => Time.now, :extra => 'd.com extra'})
      ]
  end

  def test_save_all
    ls = Linkrawler::Linkstore::LoggerLinkstore.new(nil)
    assert !ls.save_all(@links), "Links should not be saved if logger if nil"

    logger = DummyLogger.new(STDOUT)
    ls = Linkrawler::Linkstore::LoggerLinkstore.new(logger)
    ls.save_all(@links)
    assert_equal @links.size, logger.records.size, "Links should be saved"

    links = @links + [Linkrawler::Link.new({:userid => 15, :url => 'http://d%.com', :date => Time.now, :extra => 'd.com extra'})]
    assert !ls.save_all(links), "Links should not be saved"
  end

  def test_save_all!
    ls = Linkrawler::Linkstore::LoggerLinkstore.new(nil)
    assert_raise Linkrawler::Linkstore::LinkstoreUnavailableError, "Links should not be saved if logger if nil" do
      ls.save_all!(@links) 
    end

    logger = DummyLogger.new(STDOUT)
    ls = Linkrawler::Linkstore::LoggerLinkstore.new(logger)
    assert_nothing_raised "Links should be saved" do
      ls.save_all!(@links)
    end

    links = @links + [Linkrawler::Link.new({:userid => 15, :url => 'http://d%.com', :date => Time.now, :extra => 'd.com extra'})]
    assert_raise Linkrawler::Linkstore::LinkSaveError, "Links should not be saved" do
      ls.save_all!(links)
    end

    begin
      ls.save_all!(links)
    rescue Linkrawler::Linkstore::LinkSaveError => e
      assert_equal 5, e.saved.size, "Saved links should be correct"
      assert_equal 1, e.failed.size, "Failed links should be correct"
    end
  end
end
