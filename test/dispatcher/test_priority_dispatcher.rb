require File.expand_path(File.dirname(__FILE__)) + '/../test_helper'

# A dummy task class for tests
class DummyTask 
  attr_writer :bootstrap
  attr_accessor :number

  def initialize(number, bootstrap)
    @number = number
    @bootstrap = bootstrap
  end

  def run
    @number += 2
  end

  def bootstrap?
    @bootstrap
  end

end

class TestPriorityDispatcher < Test::Unit::TestCase
  def setup
    @tasks = []
    @tasks << DummyTask.new(0,true)\
           << DummyTask.new(1,true)\
           << DummyTask.new(2,false)\
           << DummyTask.new(3,false)\
           << DummyTask.new(4,false)
  end

  def test_new
    dispatcher = Linkrawler::Dispatcher::PriorityDispatcher.new(30*60)
    assert_equal 0, dispatcher.high_q.size, "high queue should be empty"
    assert_equal 0, dispatcher.low_q.size, "low queue should be empty"

    dispatcher = Linkrawler::Dispatcher::PriorityDispatcher.new(30*60,@tasks)
    assert_equal 2, dispatcher.high_q.size, "high queue should have correct tasks"
    assert_equal 3, dispatcher.low_q.size, "low queue should has correct tasks"
  end

  def test_add_tasks
    dispatcher = Linkrawler::Dispatcher::PriorityDispatcher.new(30*60)
    dispatcher.add_tasks(@tasks)
    assert_equal 2, dispatcher.high_q.size, "high queue should have correct tasks"
    assert_equal 3, dispatcher.low_q.size, "low queue should has correct tasks"

    dispatcher = Linkrawler::Dispatcher::PriorityDispatcher.new(30*60)
    dispatcher.add_tasks(DummyTask.new(1,false))
    assert_equal 0, dispatcher.high_q.size, "high queue should have correct tasks"
    assert_equal 1, dispatcher.low_q.size, "low queue should has correct tasks"
    dispatcher << DummyTask.new(1,true)
    assert_equal 1, dispatcher.high_q.size, "high queue should have correct tasks"
    assert_equal 1, dispatcher.low_q.size, "low queue should has correct tasks"
  end

  def test_dispatch
    dispatcher = Linkrawler::Dispatcher::PriorityDispatcher.new(30*60)
    assert !dispatcher.running, "Dispatcher shoud not be running"
    dispatcher.dispatch(Linkrawler::ThreadPool::FixedThreadPool.new(2))
    assert dispatcher.running, "Dispatcher should be running"

    dispatcher.add_tasks(@tasks)
    sleep 0.5
    dispatcher.mutex.synchronize do
      assert_equal 2, dispatcher.high_q.size, "high queue should have correct tasks"
      assert_equal 3, dispatcher.low_q.size, "low queue should has correct tasks"
    end
    @tasks.each_index do |i|
      assert_not_equal i, @tasks[i].number, "high-priority task should run" if dispatcher.send('high?',@tasks[i])
      assert_equal i, @tasks[i].number, "low-priorty task should not run" unless dispatcher.send('high?',@tasks[i])
    end

    @tasks[0].bootstrap = false
    @tasks[1].bootstrap = false
    sleep 0.5
    dispatcher.mutex.synchronize do
      assert_equal 5, dispatcher.low_q.size, "all tasks should be low priority"
    end
    assert_equal 'sleep', dispatcher.thread.status, "thread should be sleeping"
    
    dispatcher << DummyTask.new(10,true)
    dispatcher.mutex.synchronize do
      assert_equal 1, dispatcher.high_q.size, "should have a high priority task"
    end
    assert_equal 'run', dispatcher.thread.status, "thread should be running"
  end
end

