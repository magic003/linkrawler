require File.expand_path(File.dirname(__FILE__)) + '/../test_helper'
require 'time'

class TestReaditlaterTask < Test::Unit::TestCase
  def setup
    @apikey = ''
    @username = ''
    @password = ''
  end

  def test_run
    task = Linkrawler::Tasks::ReaditlaterTask.new(@apikey,@username,@password,1,
                                                  Time.at(1322387964))
    link_store = MockLinkStore.new
    task.add_link_store(link_store)
    link_num = 0
    task.after_save do |links|
      link_num = links.size
    end
    task.run
    assert link_store.links.size > 0, "Should have some links"
    assert_equal link_num, link_store.links.size, "Link number should be the same"
  end
end
