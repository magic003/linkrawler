require File.expand_path(File.dirname(__FILE__)) + '/../test_helper'
require 'sequel_core'

class TestDatabaseLoader < Test::Unit::TestCase
  include Linkrawler::Tasks
  include Linkrawler::Clients

  def setup
    @database = TestDbInfo::DATABASE
    @host = TestDbInfo::HOST
    @port = TestDbInfo::PORT
    @user = TestDbInfo::USER
    @password = TestDbInfo::PASSWORD
    @db = Sequel.mysql2(:database => @database, :host => @host,
      :port => @port, :user => @user, :password => @password)
    @db.create_table DatabaseLoader::TABLE do
      primary_key DatabaseLoader::CO_ID
      Integer DatabaseLoader::CO_USER_ID, :null => false
      String DatabaseLoader::CO_PROVIDER, :null => false
      String DatabaseLoader::CO_TOKEN
      String DatabaseLoader::CO_SECRET
      String DatabaseLoader::CO_UID
      String DatabaseLoader::CO_USERNAME
      String DatabaseLoader::CO_SINCE
    end
    @ds = @db[DatabaseLoader::TABLE]
    @ds.insert_multiple([
      {DatabaseLoader::CO_ID => 1, DatabaseLoader::CO_USER_ID => 1,
      DatabaseLoader::CO_PROVIDER => TWITTER, DatabaseLoader::CO_TOKEN => 'xx',
      DatabaseLoader::CO_SECRET => 'xx', DatabaseLoader::CO_USERNAME => 'yy'},
      {DatabaseLoader::CO_ID => 2, DatabaseLoader::CO_USER_ID => 1,
      DatabaseLoader::CO_PROVIDER => FACEBOOK, DatabaseLoader::CO_TOKEN => 'x'
      },
      {DatabaseLoader::CO_ID => 3, DatabaseLoader::CO_USER_ID => 1,
      DatabaseLoader::CO_PROVIDER => DELICIOUS, 
      DatabaseLoader::CO_USERNAME => 'xxxx' }, 
      {DatabaseLoader::CO_ID => 4, DatabaseLoader::CO_USER_ID => 1,
      DatabaseLoader::CO_PROVIDER => READITLATER, 
      DatabaseLoader::CO_TOKEN => 'xxxx', DatabaseLoader::CO_SECRET => 'yyy'
      },
      {DatabaseLoader::CO_ID => 5, DatabaseLoader::CO_USER_ID => 2,
      DatabaseLoader::CO_PROVIDER => READITLATER, 
      DatabaseLoader::CO_TOKEN => 'xxxx', DatabaseLoader::CO_SECRET => 'yyy',
      DatabaseLoader::CO_SINCE => Time.now.to_i.to_s
      }
      ])
  end

  def teardown
    @db.drop_table DatabaseLoader::TABLE if @db.table_exists?(DatabaseLoader::TABLE)
    @db.disconnect
  end

  def test_load
    loader = DatabaseLoader.new(@database,@user,'wrongpass',@host,@port)
    assert_raise TaskLoadError, "Should failed if not connected to database" do 
      loader.load
    end

    loader = DatabaseLoader.new(@database,@user,@password,@host,@port, 
      {:twitter => {:consumer_key => 'xxx', :consumer_secret => 'yyy'},
       :readitlater => {:apikey => 'zzz'}
      })
    assert_equal @ds.count, loader.load.size, "Tasks should be loaded"

    @ds.delete
    assert_equal @ds.count, loader.load.size, "Tasks should be empty"

    teardown
    assert_raise TaskLoadError, "Should failed if table doesn't exist" do
      loader.load
    end
  end
end
