require File.expand_path(File.dirname(__FILE__)) + '/../test_helper'

class TestFacebookTask < Test::Unit::TestCase
  def setup
    @access_token = ''
  end

  def test_run
    task = Linkrawler::Tasks::FacebookTask.new(@access_token,1)
    link_store = MockLinkStore.new
    task.add_link_store(link_store)
    link_num = 0
    task.after_save do |links|
      link_num = links.size
    end
    task.run
    assert link_store.links.size > 0, "Should have some links"
    assert_equal link_num, link_store.links.size, "Link number should be the same"
  end
end

