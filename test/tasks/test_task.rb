require 'time'
require File.expand_path(File.dirname(__FILE__)) + '/../test_helper'

# Open class Task for testing
module Linkrawler ; module Tasks
  class Task
    attr_reader :legacy_links
  end
end ; end

module Linkrawler ; module Linkstore
  class LoggerLinkstore < Base
    attr_accessor :link_logger
  end
end ; end

class TestTask < Test::Unit::TestCase
  def test_expand_url
    arch_url = 'http://www.archlinux.org/'
    assert_equal arch_url, Linkrawler::Tasks::Task.expand_url(arch_url),
                  "Arch url should not be expanded."

    short_url = 'http://goo.gl/Zm1I'
    assert_not_equal short_url, Linkrawler::Tasks::Task.expand_url(short_url),
                  "Short url should be expanded."
  end

  def test_bootstrap?
    task = Linkrawler::Tasks::Task.new(1)
    assert task.bootstrap?

    task = Linkrawler::Tasks::Task.new(1,Time.now)
    assert !task.bootstrap?
  end

  def test_save
    task = Linkrawler::Tasks::Task.new(1)
    ls = Linkrawler::Linkstore::LoggerLinkstore.new(nil)
    task.add_link_store(ls)

    links = [
        Linkrawler::Link.new({:url => 'http://a.com', :date => Time.now, :extra => 'a.com extra', :extra_id_str => '12345678'}),
        Linkrawler::Link.new({:userid => 10, :date => Time.now, :extra => 'extra', :extra_id_str => '12345679'}),
        Linkrawler::Link.new({:userid => 11, :url => 'http://b.com', :extra => 'b.com extra', :extra_id_str => '12345680'}),
        Linkrawler::Link.new({:userid => 12, :url => 'http://c.com', :date => Time.now, :extra_id_str => '12345681'}),
        Linkrawler::Link.new({:userid => 13, :url => 'http://d.com', :date => Time.now, :extra => 'd.com extra'})
      ]
    
    assert !task.send(:save,links), "Links should not be saved"
    assert_equal 5, task.legacy_links.size, "All links should be legacy"

    ls.link_logger = DummyLogger.new(STDOUT)
    assert task.send(:save,links), "Links should be saved"
    assert_equal 10, ls.link_logger.records.size, "All links should be saved"
    assert_equal 0, task.legacy_links.size, "All legacy links should be saved"

    ls.link_logger = nil
    task.send(:save,links)
    ls.link_logger = DummyLogger.new(STDOUT)
    assert !task.send(:save, [Linkrawler::Link.new({:userid => 15, :url => 'http://d%.com', :date => Time.now, :extra => 'd.com extra'})]), "The new link should not be saved"
    assert_equal 5, ls.link_logger.records.size, "Legacy links should be saved"
    assert_equal 1, task.legacy_links.size, "The new link should be legacy"
  end
end
