$:.unshift(File.dirname(__FILE__)+'/../lib') unless
  $:.include?(File.dirname(__FILE__)+'/../lib') || $:.include?(File.expand_path(File.dirname(__FILE__)+'/../lib'))

require 'test/unit'
require 'linkrawler'

# This is a mock link store, which keeps links in an array.
class MockLinkStore
  attr_reader :links

  def save_all(links)
    @links = links
  end

  def save_all!(links)
    @links = links
  end
end

# A dummy logger for testing
class DummyLogger < Logger
  attr_reader :records

  def info(message)
    raise StandardError, "contains special characters" if message.include?('%')
    @records ||= []
    @records << message
  end
end

# Open the PriorityDispatcher class for test.
module Linkrawler ; module Dispatcher
  class PriorityDispatcher
    attr_reader :high_q, :low_q, :running, :thread, :mutex
  end
end ; end

module TestDbInfo
  DATABASE = 'linkrawler_test'
  HOST = 'localhost'
  PORT = 3306
  USER = 'linkrawler_test'
  PASSWORD = 'linkrawler123'
end
