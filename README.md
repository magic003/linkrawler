# Linkrawler #

Linkrawler is a crawler that retrieves links from various link services.

## Supported link services ##
* [Delicious](http://delicious.com/help/feeds)
* [Twitter](https://dev.twitter.com/)
* [Facebook](http://developers.facebook.com/)
* [Readitlater](http://readitlaterlist.com/api/)

## Run unit tests ##

Following configurations should be done before running the unit tests:

* Clients
    * Readitlater: set *apikey*, *username* and *password* in *setup* method
    * Facebook: set *access_token* in *setup* method
    * Twitter set *consumer_key*, *consumer_secret*, *token*, *token_secret* and *screen_name* in *setup* method
* Linkstore:
    * DatabaseLinkstore: create database for test. Refer to the *setup* method.
* Tasks
    * DatabaseLoader: same as DatabaseLinkstore
    * Client tasks: same as Clients
* DbHelper: same as DatabaseLinkstore

Run unit tests:
    
```
#!shell

$ rake test
```