#!/usr/bin/env ruby
# encoding: utf-8

# This script implements the work flow for starting a Linkrawler application,
# using the libraries and framework in the gem.
#
# Usage:
#   linkrawler [options]
#   Options:
#     -c, --cfg-file FILE   Specify the YAML configuration file
#     -h, --help            Display this help message
#     -v, --version         Show Linkrawler version
#
# Author::      Minjie Zha (mailto:minjiezha@gmail.com)
# Copyright::   Copyright (c) 2011-2012 Minjie Zha

require 'optparse'
require 'yaml'
require 'logger'
require 'rack'

begin
  require 'linkrawler'
rescue LoadError
  $:.unshift File.join(File.dirname(__FILE__), *%w[.. lib])
  retry
end

# Constants 
CFG_FILE_OPT = 'cfg-file'

# Parse command line options
options = {}
opts = OptionParser.new do |opts|
  opts.banner = "Usage: linkrawler [options]"

  opts.separator ""
  opts.separator "Options:"
  
  opts.on('-c','--cfg-file FILE','Specify the YAML configuration file') do |cfg|
    options[CFG_FILE_OPT] = cfg
  end

  opts.on_tail('-h','--help','Display this help message') do 
    puts opts
    exit
  end

  opts.on_tail('-v','--version','Show Linkrawler version') do
    puts Linkrawler::VERSION
    exit
  end
end

# Read command line options into `options` hash
opts.parse!

# Load configuration parameters
default_cfg = {
}

if options.key?(CFG_FILE_OPT)
  cfg = YAML::load(File.open(options[CFG_FILE_OPT]))
  default_cfg.merge!(cfg)
end

puts "Setting application logger..."
logger_cfg = default_cfg['linkrawler_logger']
Linkrawler::Log.logdev = logger_cfg['logdev'] unless logger_cfg['logdev'].nil?
Linkrawler::Log.shift_age = logger_cfg['shift_age'] unless logger_cfg['shift_age'].nil?
Linkrawler::Log.shift_size = logger_cfg['shift_size'] unless logger_cfg['shift_size'].nil?
Linkrawler::Log.level = eval(logger_cfg['level']) unless logger_cfg['level'].nil?

puts "Creating link stores..."
logger = Logger.new(default_cfg['logger_linkstore']['logdev'],
  default_cfg['logger_linkstore']['shift_age'],
  default_cfg['logger_linkstore']['shift_size'])

logger_linkstore = Linkrawler::Linkstore::LoggerLinkstore.new(logger)

db_cfg = default_cfg['database_linkstore']
database_linkstore = Linkrawler::Linkstore::DatabaseLinkstore.new(
  db_cfg['database'], db_cfg['user'], db_cfg['password'], 
  db_cfg['host'], db_cfg['port'])

puts "Loading tasks from database..."
db_cfg = default_cfg['database_loader']
app_keys = {
  :twitter => {
    :consumer_key => default_cfg['twitter']['consumer_key'], 
    :consumer_secret => default_cfg['twitter']['consumer_secret'] 
  },
  :readitlater => {:apikey => default_cfg['readitlater']['apikey'], :encrypt => {:key => default_cfg['readitlater']['encrypt']['key'], :iv => default_cfg['readitlater']['encrypt']['iv'] } }
}
factory = Linkrawler::Tasks::DatabaseTaskFactory.new(db_cfg['database'],
  db_cfg['user'], db_cfg['password'], db_cfg['host'], db_cfg['port'],
  app_keys, [logger_linkstore, database_linkstore])
loader = Linkrawler::Tasks::DatabaseLoader.new(factory)

tasks = loader.load

puts "Creating thread pool..."
pool = Linkrawler::ThreadPool::FixedThreadPool.new(
  default_cfg['fixed_thread_pool']['size'])

puts "Creating the dispatcher..."
dispatcher = Linkrawler::Dispatcher::PriorityDispatcher.new(
  default_cfg['priority_dispatcher']['interval'])
dispatcher.add_tasks(tasks)
dispatcher.dispatch(pool)

puts "Linkrawler is running."

puts "Starting Restful API server..."
api = Linkrawler::Interface::RackInterface.new(dispatcher, factory)
port = default_cfg['rack_interface']['port'] || 9292
builder = Rack::Builder.new do
  use Rack::CommonLogger

  map '/task' do
    run api
  end
end
Rack::Handler::Thin.run builder, :Port => port
